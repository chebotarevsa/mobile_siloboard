import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_string_encryption/flutter_string_encryption.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mobile_siloboard/features/siloboard/data/datasources/image_datasource.dart';
import 'package:mobile_siloboard/features/siloboard/data/datasources/impl/image_datasource_impl.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/widget/elevotor_details.dart';
import 'package:mobile_siloboard/l10n/localization.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:package_info/package_info.dart';

import 'package:mobile_siloboard/features/logon/data/repositories/logon_repository_impl.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/data/datasources/connection_server_datasource.dart';
import 'package:mobile_siloboard/features/logon/data/datasources/registration_persist_datasource.dart';
import 'package:mobile_siloboard/features/logon/presentation/screen/passcode_create_screen.dart';
import 'package:mobile_siloboard/features/logon/presentation/screen/registration_screen.dart';
import 'package:mobile_siloboard/features/logon/presentation/screen/splash_screen.dart';
import 'package:mobile_siloboard/features/settings/data/datasource/application_setting_datasource.dart';
import 'package:mobile_siloboard/features/settings/data/repository/application_settings_repository_impl.dart';
import 'package:mobile_siloboard/features/settings/presentation/screen/appliacation_settings_screen.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/screen/elevator_list.dart';
import 'package:mobile_siloboard/features/logon/data/repositories/registration_repository_impl.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/features/logon/presentation/screen/passcode_validate_screen.dart';
import 'package:mobile_siloboard/features/settings/domian/repository/application_settings_repository.dart';

import 'features/siloboard/data/datasources/impl/odata_remote_datasource_impl.dart';
import 'features/siloboard/data/datasources/remote_datasource.dart';
import 'features/siloboard/data/repositories/siloboard_repository_impl.dart';
import 'features/siloboard/domain/repositories/siloboard_repository.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  GetIt.I.registerSingleton<RegistrationEncryptedPersistDataSource>(
      CredentialEncryptedStoreSharedPreferencesDataSource(
          json: json,
          cryptor: PlatformStringCryptor(),
          sharedPreferences: await SharedPreferences.getInstance()));

  GetIt.I.registerSingleton<AssetsApplicationSettingDataSource>(
      AssetsApplicationSettingDataSource(rootBundle));

  GetIt.I.registerSingleton<SharedPreferencesApplicationSettingDataSource>(
      SharedPreferencesApplicationSettingDataSource(
          await SharedPreferences.getInstance()));

  GetIt.I.registerSingleton<ApplicationSettingsRepository>(
      ApplicationSettingsRepositoryImpl(
          currentApplicationSettingDataSource:
              GetIt.I<SharedPreferencesApplicationSettingDataSource>(),
          defaultApplicationSettingDataSource:
              GetIt.I<AssetsApplicationSettingDataSource>()));

  GetIt.I.registerSingleton<ConnectionServerDataSource>(
      ConnectionSMPServerDataSource(
          client: Client(),
          applicationSettingsRepository:
              GetIt.I<ApplicationSettingsRepository>()));

  GetIt.I.registerSingleton<RegistrationRepository>(RegistrationRepositoryImpl(
      connectionServerDataSource: GetIt.I<ConnectionServerDataSource>(),
      registrationEncryptedPersistDataSource:
          GetIt.I<RegistrationEncryptedPersistDataSource>()));

  GetIt.I.registerSingleton<LogonRepository>(LogonRepositoryImpl());
  GetIt.I.registerSingleton<PackageInfo>(await PackageInfo.fromPlatform());

  GetIt.I.registerSingleton<RemoteDataSource>(
    ODataRemoteDataSourceImpl(
      applicationSettingsRepository: GetIt.I<ApplicationSettingsRepository>(),
      logonRepository: GetIt.I<LogonRepository>(),
    ),
  );

  GetIt.I.registerSingleton<ImageDataSource>(AssetImageDataSourceImpl());
  GetIt.I.registerSingleton<SiloboardRepository>(SiloboardRepositoryImpl(
    remoteDataSource: GetIt.I<RemoteDataSource>(),
    imageDataSource: GetIt.I<ImageDataSource>(),
  ));

  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(
    MultiRepositoryProvider(
      providers: [
        RepositoryProvider<RegistrationRepository>(
          create: (context) => GetIt.I<RegistrationRepository>(),
        ),
        RepositoryProvider<ApplicationSettingsRepository>(
          create: (context) => GetIt.I<ApplicationSettingsRepository>(),
        ),
        RepositoryProvider<LogonRepository>(
          create: (context) => GetIt.I<LogonRepository>(),
        ),
        RepositoryProvider<PackageInfo>(
          create: (context) => GetIt.I<PackageInfo>(),
        ),
        RepositoryProvider<SiloboardRepository>(
          create: (context) => GetIt.I<SiloboardRepository>(),
        ),
      ],
      child: SiloboardApp(),
    ),
  );
}

class SiloboardApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        AppLocalization.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('ru'),
      ],
      title: 'Siloboard',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => SplashScreen(),
        '/settings': (context) => ApplicationSetting(),
        '/logon/register': (context) => RegistrationScreen(),
        '/logon/validate_passcode': (context) => PasscodeValidateScreen(),
        '/logon/create_passcode': (context) => PasscodeCreateScreen(),
        '/silobiard': (context) => ElevatorList(),
        '/siloboard/elevator': (context) => ElevatorDetails()
      },
    );
  }
}
