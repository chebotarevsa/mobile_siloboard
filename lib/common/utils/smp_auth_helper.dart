import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';

class SMPAuthHelper {
  static String getBasicAuth({
    @required String username,
    @required String password,
  }) {
    final token = base64.encode(utf8.encode('$username:$password'));
    final authStr = 'Basic ' + token.trim();
    return authStr;
  }

  static Map<String, String> getSMPAuthHeaders({
    @required Registration registration,
  }) {
    return {
      'X-SMP-APPCID': registration.appCId,
      'Authorization': getBasicAuth(
        username: registration.username,
        password: registration.password,
      )
    };
  }
}
