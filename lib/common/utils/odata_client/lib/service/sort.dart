import 'package:equatable/equatable.dart';

class Sort extends Equatable {

  final List<String> sorts;

  Sort(this.sorts);

  @override
  List<Object> get props => [sorts];

}