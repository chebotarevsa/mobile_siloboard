
abstract class Filter {}

class FilterLogic extends Filter {
  final Filter left;
  final Filter right;
  final String operation;

  @override
  String toString() {
    if (left == null) {
      return '$operation $right';
    } else {
      return '$left $operation $right';
    }
  }

  FilterLogic._(this.left, this.right, this.operation);

  factory FilterLogic.eq(Filter left, Filter right) {
    return FilterLogic._(left, right, 'eq');
  }

  factory FilterLogic.ne(Filter left, Filter right) {
    return FilterLogic._(left, right, 'ne');
  }

  factory FilterLogic.gt(Filter left, Filter right) {
    return FilterLogic._(left, right, 'gt');
  }

  factory FilterLogic.ge(Filter left, Filter right) {
    return FilterLogic._(left, right, 'ge');
  }

  factory FilterLogic.lt(Filter left, Filter right) {
    return FilterLogic._(left, right, 'lt');
  }

  factory FilterLogic.le(Filter left, Filter right) {
    return FilterLogic._(left, right, 'le');
  }

  factory FilterLogic.and(FilterLogic left, FilterLogic right) {
    return FilterLogic._(left, right, 'and');
  }

  factory FilterLogic.andAll(List<FilterLogic> logic) {
    return logic.reduce((value, element) => FilterLogic.and(value, element));
  }

  factory FilterLogic.or(FilterLogic left, FilterLogic right) {
    return FilterLogic._(left, right, 'or');
  }

  factory FilterLogic.orAll(List<FilterLogic> logic) {
    return logic.reduce((value, element) => FilterLogic.or(value, element));
  }

  factory FilterLogic.not(FilterLogic right) {
    return FilterLogic._(null, right, 'not');
  }
}

class FilterMath extends Filter {
  final FilterProperty property;
  final FilterValue<num> value;
  final String operation;

  FilterMath._(this.property, this.value, this.operation);

  @override
  String toString() {
    return '$property $operation $value';
  }

  factory FilterMath.add(FilterProperty property, FilterValue<num> value) {
    return FilterMath._(property, value, 'add');
  }
  factory FilterMath.sub(FilterProperty property, FilterValue<num> value) {
    return FilterMath._(property, value, 'sub');
  }
  factory FilterMath.mul(FilterProperty property, FilterValue<num> value) {
    return FilterMath._(property, value, 'mul');
  }
  factory FilterMath.div(FilterProperty property, FilterValue<num> value) {
    return FilterMath._(property, value, 'div');
  }
  factory FilterMath.mod(FilterProperty property, FilterValue<num> value) {
    return FilterMath._(property, value, 'mode');
  }
}

class FilterProperty extends Filter {
  final String property;
  FilterProperty._(this.property);
  @override
  String toString() {
    return property;
  }

  factory FilterProperty.name(String property) {
    return FilterProperty._(property);
  }
}

class FilterGroup extends Filter {
  final Filter group;

  FilterGroup._(this.group);

  @override
  String toString() {
    return '($group)';
  }

  factory FilterGroup.group(Filter group) {
    return FilterGroup._(group);
  }
}

class FilterValue<T> extends Filter {
  final T value;
  FilterValue._(this.value);

  @override
  String toString() {
    if (value is int) {
      return '$value';
    }
    if (value is double) {
      return '${value}d';
    }
    return '\'$value\'';
  }

  factory FilterValue.value(T value) {
    return FilterValue._(value);
  }
}

class FilterFunction extends Filter {
  final Filter arg1;
  final Filter arg2;
  final Filter arg3;
  final String functionName;

  FilterFunction._(this.functionName, this.arg1, this.arg2, this.arg3);

  @override
  String toString() {
    if (arg2 == null && arg3 == null) {
      return '$functionName($arg1)';
    } else if (arg3 == null) {
      return '$functionName($arg1, $arg2)';
    } else {
      return '$functionName($arg1, $arg2, $arg3)';
    }
  }

  factory FilterFunction.substringof(FilterProperty property, FilterValue arg) {
    return FilterFunction._('substringof', property, arg, null);
  }
}
