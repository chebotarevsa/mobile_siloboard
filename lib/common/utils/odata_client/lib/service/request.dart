class Request{
   Map<String, dynamic> execute(){
     return null;
   }
}

class GetEntityRequest extends Request{
    final String entitySetName;
    final dynamic key;
    final dynamic keys;

  GetEntityRequest(this.entitySetName, this.key, this.keys);
}


class GetEntitiesRequest{
  final String entitySetName;

  GetEntitiesRequest(this.entitySetName);

}