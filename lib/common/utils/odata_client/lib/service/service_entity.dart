import 'package:mobile_siloboard/common/utils/odata_client/lib/service/request.dart';

class ServiceEntity{
  final String name;

  ServiceEntity(this.name);

   Request getEntity({ dynamic key, Map<String, dynamic> keys}){
      return GetEntityRequest(name, key, keys);
   }

   getEntities(){

   }

   createEntity(){

   }

   updateEntity(){

   }

   deleteEntity({ dynamic key, Map<String, dynamic> keys}){

   }

}