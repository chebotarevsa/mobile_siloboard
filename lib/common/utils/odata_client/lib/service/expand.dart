import 'package:equatable/equatable.dart';

class Expand extends Equatable {

  final List<String> expands;

  Expand(this.expands);

  @override
  List<Object> get props => [expands];

}