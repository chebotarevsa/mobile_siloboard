import 'package:equatable/equatable.dart';
import 'package:mobile_siloboard/common/utils/odata_client/lib/model/property.dart';

class EdmEntity extends Equatable {
  final String name;
  final List<EdmProperty> properties;
  final List<EdmProperty> keys;

  EdmEntity(this.name, this.properties, this.keys);

  @override
  List<Object> get props => [name, properties];
}
