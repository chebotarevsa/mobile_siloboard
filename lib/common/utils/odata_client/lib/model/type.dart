import 'package:equatable/equatable.dart';

abstract class EdmType extends Equatable {}

class EdmBinary extends EdmType {
  final String type = 'Edm.Binary';

  @override
  List<Object> get props => [type];
}

class EdmString extends EdmType {
  final String type = 'Edm.String';
  final int length;

  EdmString({this.length});

  @override
  List<Object> get props => [type];
}
