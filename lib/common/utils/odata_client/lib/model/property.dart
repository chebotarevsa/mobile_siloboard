import 'package:equatable/equatable.dart';
import 'package:mobile_siloboard/common/utils/odata_client/lib/model/type.dart';

class EdmProperty extends Equatable{
  final String name;
  final EdmType type;
  final int length;

  EdmProperty(this.name, this.type, this.length);

  @override
  List<Object> get props => [name, type, length];

}