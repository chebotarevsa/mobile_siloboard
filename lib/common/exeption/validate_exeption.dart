class ValidateExeption implements Exception{
  final String message;

  ValidateExeption(this.message);

  @override
  String toString() {
    return this.message ?? super.toString();
  }

}