import 'package:equatable/equatable.dart';

class Failure extends Equatable {
  final String message;
  final Exception reason;

  Failure(this.message, {this.reason});

  @override
  String toString() {
    return this.message;
  }

  @override
  List<Object> get props => [message, reason];
}