import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_siloboard/features/settings/domian/bloc/bloc.dart';
import 'package:mobile_siloboard/l10n/localization.dart';

class ApplicationSettingForm extends StatefulWidget {
  @override
  State<ApplicationSettingForm> createState() => _ApplicationSettingStateForm();
}

class _ApplicationSettingStateForm extends State<ApplicationSettingForm> {
  SettingsBloc _settingsBloc;
  bool _isHttps;
  final _appIdController = TextEditingController();
  final _hostController = TextEditingController();
  final _postController = TextEditingController();
  final _appIdFocus = new FocusNode();
  final _hostFocus = new FocusNode();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void initState() {
    super.initState();
    _settingsBloc = BlocProvider.of<SettingsBloc>(context);
    _settingsBloc.add(SettingsLoad());
    _appIdController.addListener(_onChangeAppId);
    _hostController.addListener(_onChangeHost);
    _postController.addListener(_onChangePort);
  }

  @override
  void dispose() {
    _appIdController.dispose();
    _hostController.dispose();
    _postController.dispose();
    super.dispose();
  }

  _onHTTPSChange(value) {
    _settingsBloc.add(
      SettingsHttpsChanged(https: value),
    );
  }

  _onChangeAppId() {
    _settingsBloc.add(
      SettingsAppIdChanged(appId: _appIdController.text),
    );
  }

  _onChangeHost() {
    _settingsBloc.add(
      SettingsHostChanged(host: _hostController.text),
    );
  }

  _onChangePort() {
    _settingsBloc.add(
      SettingsPortChanged(
        port: int.parse(_postController.text),
      ),
    );
  }

  _showErrorDialog(context, text) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(text),
        backgroundColor: Colors.red,
      ),
    );
  }

  _showSuccessDialog(context, text) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(text),
        backgroundColor: Colors.green,
      ),
    );
  }

  _submitSettings() {
    _settingsBloc.add(SettingsSubmit(
      https: _isHttps,
      port: int.parse(_postController.text),
      host: _hostController.text,
      appId: _appIdController.text,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SettingsBloc, SettingsState>(
      listener: (context, state) {
        if (state.loadError) {
          _showErrorDialog(
            context,
            AppLocalization.of(context)
                .settingsMessageSettingsDoNotLoaded(),
          );
        }
        if (state.loaded) {
          _appIdController.text = state.appId;
          _hostController.text = state.host;
          _postController.text = state.port.toString();
          _isHttps = state.https;
          _showSuccessDialog(
            context,
            AppLocalization.of(context).settingsMessageLoaded(),
          );
        }
        if (state.submitError) {
          _showErrorDialog(
            context,
            AppLocalization.of(context).settingsMessageSettingsDoNotSaved(),
          );
        }
        if (state.submitted) {
          _showSuccessDialog(
            context,
            AppLocalization.of(context).settingsMessageSaved(),
          );
        }
      },
      child: BlocBuilder<SettingsBloc, SettingsState>(
        builder: (context, state) {
          _isHttps = state.https;
          return Scaffold(
            key: _scaffoldKey,
            floatingActionButton: FloatingActionButton(
              child: Icon(
                Icons.save,
              ),
              backgroundColor: state.isValidForm
                  ? Theme.of(context).primaryColor
                  : Theme.of(context).primaryColorLight,
              onPressed: state.isValidForm ? _submitSettings : null,
            ),
            appBar: AppBar(
              title: Text(
                AppLocalization.of(context).settingsTextSettings(),
              ),
            ),
            body: Container(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10,
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: AppLocalization.of(context)
                                .settingsLabelApplicationId(),
                            icon: Icon(Icons.settings_applications),
                          ),
                          controller: _appIdController,
                          focusNode: _appIdFocus,
                          autovalidate: true,
                          validator: (value) => state.isValidAppId
                              ? null
                              : AppLocalization.of(context)
                                  .settingsMessageApplicationIdIsEmpty(),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: AppLocalization.of(context)
                                .settingsLabelHost(),
                            icon: Icon(Icons.computer),
                          ),
                          controller: _hostController,
                          focusNode: _hostFocus,
                          autovalidate: true,
                          validator: (value) => state.isValidHost
                              ? null
                              : AppLocalization.of(context)
                                  .settingsMessageHostIdIsEmpty(),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: AppLocalization.of(context)
                                .settingsLabelPort(),
                            icon: Icon(
                              Icons.settings_input_svideo,
                            ),
                          ),
                          controller: _postController,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                          autovalidate: true,
                          validator: (value) => state.isValidPort
                              ? null
                              : AppLocalization.of(context)
                                  .settingsMessagePortIncorrent(),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                    child: Icon(
                                      Icons.security,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Text(
                                    AppLocalization.of(context)
                                        .settingsLabelHTTPS(),
                                  ),
                                ],
                              ),
                              Checkbox(
                                onChanged: _onHTTPSChange,
                                value: _isHttps,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
