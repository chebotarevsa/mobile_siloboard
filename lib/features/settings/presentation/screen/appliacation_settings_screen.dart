import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_siloboard/features/settings/domian/bloc/bloc.dart';
import 'package:mobile_siloboard/features/settings/domian/repository/application_settings_repository.dart';
import 'package:mobile_siloboard/features/settings/presentation/widget/application_settings_form.dart';

class ApplicationSetting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SettingsBloc>(
      create: (context) {
        return SettingsBloc(
          applicationSettingsRepository:
              RepositoryProvider.of<ApplicationSettingsRepository>(context),
        );
      },
      child: ApplicationSettingForm()
    );
  }
}
