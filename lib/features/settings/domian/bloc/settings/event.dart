import 'package:equatable/equatable.dart';

abstract class SettingsEvent extends Equatable {
  const SettingsEvent();

  @override
  List<Object> get props => [];
}

class SettingsSubmit extends SettingsEvent {
  final String appId;
  final String host;
  final int port;
  final bool https;

  const SettingsSubmit({this.appId, this.host, this.port, this.https});

  @override
  List<Object> get props => [appId, host, port, https];
}

class SettingsAppIdChanged extends SettingsEvent {
  final String appId;

  const SettingsAppIdChanged({this.appId});

  @override
  List<Object> get props => [appId];
}

class SettingsHostChanged extends SettingsEvent {
  final String host;

  const SettingsHostChanged({
    this.host,
  });

  @override
  List<Object> get props => [host];
}

class SettingsPortChanged extends SettingsEvent {
  final int port;

  const SettingsPortChanged({
    this.port,
  });

  @override
  List<Object> get props => [port];
}

class SettingsHttpsChanged extends SettingsEvent {
  final bool https;

  const SettingsHttpsChanged({this.https});

  @override
  List<Object> get props => [https];
}



class SettingsResetSubmitError extends SettingsEvent {}
class SettingsResetLoadError extends SettingsEvent {}

class SettingsLoad extends SettingsEvent {}
