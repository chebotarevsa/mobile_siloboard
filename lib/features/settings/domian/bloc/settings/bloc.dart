import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/settings/domian/bloc/settings/event.dart';
import 'package:mobile_siloboard/features/settings/domian/bloc/settings/state.dart';
import 'package:mobile_siloboard/features/settings/domian/entity/application_settings.dart';
import 'package:mobile_siloboard/features/settings/domian/repository/application_settings_repository.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  static const String EMPTY = '';
  static const int MIN_PORT = 0;
  static const int MAX_PORT = 65535;

  final ApplicationSettingsRepository applicationSettingsRepository;

  SettingsBloc({@required this.applicationSettingsRepository})
      : assert(applicationSettingsRepository != null);

  @override
  SettingsState get initialState => SettingsState.init();

  @override
  Stream<SettingsState> mapEventToState(SettingsEvent event) async* {
    if (event is SettingsLoad) {
      yield state.copyWith(loadStart: true);
      try {
        final setting =
            await applicationSettingsRepository.getApplicationSettings();
        yield state.copyWith(
          appId: setting.appId,
          isValidAppId: setting.appId?.isNotEmpty,
          host: setting.host,
          isValidHost: setting.host?.isNotEmpty,
          port: setting.port,
          isValidPort: setting.port > 0,
          https: setting.https,
          loaded: true,
        );
      } catch (e) {
        yield state.copyWith(
          loadStart: false,
          loadError: true,
          loadErrorMessage: e.toString(),
        );
      }
      yield state.copyWith(
          loadStart: false,
          loadError: false,
          loaded: false,
          loadErrorMessage: EMPTY);
    }

    if (event is SettingsHttpsChanged) {
      yield state.copyWith(
        https: event.https,
      );
    }

    if (event is SettingsAppIdChanged) {
      yield state.copyWith(
        appId: event.appId,
        isValidAppId: event.appId.isNotEmpty,
      );
    }

    if (event is SettingsHostChanged) {
      yield state.copyWith(
        host: event.host,
        isValidHost: event.host.isNotEmpty,
      );
    }

    if (event is SettingsPortChanged) {
      yield state.copyWith(
        port: event.port,
        isValidPort: event.port > MIN_PORT && event.port <= MAX_PORT,
      );
    }

    if (event is SettingsSubmit) {
      if (state.isValidForm) {
        try {
          yield state.copyWith(submitStart: true);
          await applicationSettingsRepository.setApplicationSettings(
              ApplicationSettings(
                  appId: state.appId,
                  host: state.host,
                  port: state.port,
                  https: state.https));
          yield state.copyWith(submitStart: false, submitted: true);
        } catch (e) {
          yield state.copyWith(
            submitStart: false,
            submitError: true,
          );
        }
        yield state.copyWith(
          submitStart: false,
          submitError: false,
          submitted: false,
        );
      }
    }
  }
}
