import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class SettingsState extends Equatable {
  final String appId;
  final String host;
  final int port;
  final bool https;
  final bool isValidAppId;
  final bool isValidHost;
  final bool isValidPort;
  final bool submitStart;
  final bool submitError;
  final bool submitted;
  final String submitErrorMessage;
  final bool loadStart;
  final bool loadError;
  final bool loaded;
  final String loadErrorMessage;

  bool get isValidForm => isValidAppId && isValidHost && isValidPort;

  SettingsState({
    @required this.appId,
    @required this.host,
    @required this.port,
    @required this.https,
    @required this.isValidAppId,
    @required this.isValidPort,
    @required this.isValidHost,
    @required this.submitStart,
    @required this.submitError,
    @required this.submitErrorMessage,
    @required this.loadStart,
    @required this.loadError,
    @required this.loadErrorMessage,
    @required this.loaded,
    @required this.submitted,
  });

  factory SettingsState.init() {
    return SettingsState(
      appId: '',
      host: '',
      port: 0,
      https: true,
      isValidAppId: false,
      isValidPort: false,
      isValidHost: false,
      submitStart: false,
      submitError: false,
      submitErrorMessage: '',
      loadStart: false,
      loadError: false,
      loaded: false,
      loadErrorMessage: '',
      submitted: false,
    );
  }

  @override
  List<Object> get props => [
        appId,
        host,
        port,
        https,
        isValidAppId,
        isValidPort,
        isValidHost,
        submitStart,
        submitError,
        submitted,
        submitErrorMessage,
        loadStart,
        loadError,
        loadErrorMessage,
        loaded,
      ];

  SettingsState copyWith({
    String appId,
    String host,
    int port,
    bool https,
    bool isValidAppId,
    bool isValidHost,
    bool isValidPort,
    bool submitStart,
    bool submitError,
    bool submitted,
    String submitErrorMessage,
    bool loadStart,
    bool loadError,
    String loadErrorMessage,
    bool loaded,
  }) {
    return SettingsState(
      appId: appId ?? this.appId,
      host: host ?? this.host,
      port: port ?? this.port,
      https: https ?? this.https,
      isValidAppId: isValidAppId ?? this.isValidAppId,
      isValidPort: isValidPort ?? this.isValidPort,
      isValidHost: isValidHost ?? this.isValidHost,
      submitStart: submitStart ?? this.submitStart,
      submitError: submitError ?? this.submitError,
      submitted: submitted ?? this.submitted,
      submitErrorMessage: submitErrorMessage ?? this.submitErrorMessage,
      loadStart: loadStart ?? this.loadStart,
      loadError: loadError ?? this.loadError,
      loadErrorMessage: loadErrorMessage ?? this.loadErrorMessage,
      loaded: loaded ?? this.loaded,
    );
  }

  @override
  String toString() {
    return 'isValidForm: $isValidForm, loadStart: $loadStart, appId:$appId, host:$host, port:$port, https:$https';
  }
}

class SettingsStateLoad extends SettingsState {}
