import 'package:mobile_siloboard/features/settings/domian/entity/application_settings.dart';

abstract class ApplicationSettingsRepository{
   Future<ApplicationSettings> getApplicationSettings();
   Future<void> setApplicationSettings(ApplicationSettings applicationSettings);
}