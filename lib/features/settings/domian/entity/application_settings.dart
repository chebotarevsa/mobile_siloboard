import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ApplicationSettings extends Equatable {
  final String appId;
  final String host;
  final int port;
  final bool https;

  ApplicationSettings({
    @required this.host,
    @required this.port,
    @required this.appId,
    @required this.https,
  });

  @override
  List<Object> get props => [appId, host, port, https];

  ApplicationSettings copyWith(
      {String appId, String host, int port, bool https}) {
    return ApplicationSettings(
        appId: appId ?? this.appId,
        host: host ?? this.host,
        port: port ?? this.port,
        https: https ?? this.https);
  }
}
