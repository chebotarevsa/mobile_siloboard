import 'dart:convert';

import 'package:mobile_siloboard/features/settings/data/model/application_settings_model.dart';
import 'package:flutter/services.dart' show AssetBundle;
import 'package:shared_preferences/shared_preferences.dart';

abstract class ApplicationSettingDataSource {
  Future<ApplicationSettingsModel> getApplicationSettings();

  Future<void> setApplicationSettings(
      ApplicationSettingsModel applicationSettingsModel);
}

class ApplicationSettingException implements Exception {
  final String msg;

  const ApplicationSettingException([this.msg = '']);

  String toString() => '$msg';
}

class AssetsApplicationSettingDataSource
    implements ApplicationSettingDataSource {
  final AssetBundle _rootBundle;

  AssetsApplicationSettingDataSource(this._rootBundle);

  @override
  Future<ApplicationSettingsModel> getApplicationSettings() async {
    var data = await _rootBundle.loadString('assets/settings/application.json');
    var dataJson = json.decode(data);
    return ApplicationSettingsModel.fromJson(dataJson);
  }

  @override
  Future<void> setApplicationSettings(
      ApplicationSettingsModel applicationSettingsModel) {
    throw ApplicationSettingException('Method not implemented');
  }
}

class SharedPreferencesApplicationSettingDataSource
    implements ApplicationSettingDataSource {
  static const String KEY = 'APPLICATION_SETTINGS';

  final SharedPreferences _sharedPreferences;

  SharedPreferencesApplicationSettingDataSource(this._sharedPreferences);

  @override
  Future<ApplicationSettingsModel> getApplicationSettings() async {
    var data = _sharedPreferences.getString(KEY);
    if (data == null) {
      return null;
    }
    var dataJson = jsonDecode(data);
    return ApplicationSettingsModel.fromJson(dataJson);
  }

  @override
  Future<void> setApplicationSettings(
      ApplicationSettingsModel applicationSettingsModel) async {
    _sharedPreferences.setString(
        KEY, jsonEncode(applicationSettingsModel.toJson()));
  }
}
