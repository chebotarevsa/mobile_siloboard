import 'package:mobile_siloboard/features/settings/data/datasource/application_setting_datasource.dart';
import 'package:mobile_siloboard/features/settings/data/model/application_settings_model.dart';
import 'package:mobile_siloboard/features/settings/domian/entity/application_settings.dart';
import 'package:mobile_siloboard/features/settings/domian/repository/application_settings_repository.dart';

class ApplicationSettingsRepositoryImpl extends ApplicationSettingsRepository {
  final ApplicationSettingDataSource defaultApplicationSettingDataSource;
  final ApplicationSettingDataSource currentApplicationSettingDataSource;

  ApplicationSettingsRepositoryImpl({
    this.defaultApplicationSettingDataSource,
    this.currentApplicationSettingDataSource,
  })  : assert(defaultApplicationSettingDataSource != null),
        assert(currentApplicationSettingDataSource != null);

  @override
  Future<ApplicationSettings> getApplicationSettings() async {
    ApplicationSettingsModel result =
        await currentApplicationSettingDataSource.getApplicationSettings();
    if (result == null) {
      result =
          await defaultApplicationSettingDataSource.getApplicationSettings();
    }
    return ApplicationSettings(
        appId: result.appId,
        host: result.host,
        port: result.port,
        https: result.https);
  }

  @override
  Future<void> setApplicationSettings(
      ApplicationSettings applicationSettings) async {
    currentApplicationSettingDataSource.setApplicationSettings(
        ApplicationSettingsModel(
            appId: applicationSettings.appId,
            host: applicationSettings.host,
            port: applicationSettings.port,
            https: applicationSettings.https));
  }
}
