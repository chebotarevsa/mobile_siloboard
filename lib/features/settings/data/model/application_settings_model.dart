import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ApplicationSettingsModel extends Equatable {
  final String appId;
  final String host;
  final int port;
  final bool https;

  ApplicationSettingsModel({
    @required this.host,
    @required  this.port,
    @required this.appId,
    @required  this.https,
  });

  factory ApplicationSettingsModel.fromJson(Map<String, dynamic> jsonMap) {
    return ApplicationSettingsModel(
        host: jsonMap["host"],
        port: jsonMap["port"] as int,
        appId: jsonMap["appId"],
        https: (jsonMap["https"] as bool));
  }

  Map<String, dynamic> toJson() {
    return {
      "host": this.host,
      "port": this.port,
      "appId": this.appId,
      "https": this.https
    };
  }

  @override
  List<Object> get props => [appId, host, port, https];
}
