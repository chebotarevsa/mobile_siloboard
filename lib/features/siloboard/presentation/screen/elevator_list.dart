import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/widget/dialog.dart';
import './../bloc/bloc.dart';

import 'package:mobile_siloboard/features/siloboard/presentation/widget/elevator_card.dart';
import '../../domain/repositories/siloboard_repository.dart';

class ElevatorList extends StatelessWidget {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
    new GlobalKey<RefreshIndicatorState>();
  void _listener(BuildContext context, ElevatorListState state) {
    if (state.loadElevatorFailed)
      showErrorDialog(context, state.loadElevatorFailedMessage);
    if(state.loadElevatorStart){
      //_refreshIndicatorKey.currentState.show();
    }

  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ElevatorListBloc>(
      create: (BuildContext context) => ElevatorListBloc(
        siloboardRepository: RepositoryProvider.of<SiloboardRepository>(
          context,
        ),
      )..add(ElevatorListEventLoad()),
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {},
          ),
          title: Text("Элеватор"),
        ),
        body: BlocListener<ElevatorListBloc, ElevatorListState>(
          listener: this._listener,
          child: Container(
            decoration: BoxDecoration(color: Colors.white12),
            child: BlocBuilder<ElevatorListBloc, ElevatorListState>(
                builder: (context, state) {
              return RefreshIndicator(
                key: _refreshIndicatorKey,
                onRefresh: () async {
                  BlocProvider.of<ElevatorListBloc>(context)
                    ..add(ElevatorListEventRefresh());
                },
                child: ListView.builder(
                  itemBuilder: (context, position) =>
                      ElevatorCard(elevator: state.elevators[position]),
                  itemCount: state.elevators.length,
                ),
              );
            }),
          ),
        ),
      ),
    );
  }
}
