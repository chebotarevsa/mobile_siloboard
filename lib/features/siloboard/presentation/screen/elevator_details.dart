import 'package:flutter/material.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/models/ElevatorViewModel.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/widget/crop_list.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/widget/elevotor_details.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/widget/silo_list.dart';

class ElevatorDetailsScreen extends StatefulWidget {
  static const routeName = '/siloboard/elevator';
  @override
  State<StatefulWidget> createState() {
    return _ElevatorDetailsScreen();
  }
}

class _ElevatorDetailsScreen extends State<StatefulWidget> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    ElevatorDetails(),
    SiloList(),
    CropList(),
    SiloList(),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  Widget build(BuildContext context) {
    final ElevatorViewModel elevator =
        ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(elevator.name),
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        onTap: onTabTapped,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              backgroundColor: Colors.green,
              title: Text("Главная")),
          BottomNavigationBarItem(
              icon: Icon(Icons.location_city),
              backgroundColor: Colors.green,
              title: Text("Структура")),
          BottomNavigationBarItem(
              icon: Icon(Icons.spa),
              backgroundColor: Colors.green,
              title: Text("Культуры")),
          BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today),
              backgroundColor: Colors.green,
              title: Text("Журнал"))
        ],
      ),
    );
  }
}
