import 'package:flutter/material.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/models/ElevatorViewModel.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/screen/elevator_details.dart';

class ElevatorCard extends StatelessWidget {
  final ElevatorViewModel elevator;

  const ElevatorCard({Key key, this.elevator}) : super(key: key);

  Row _buildButtonColumn(Color color, IconData icon, String label) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Text(
        //   '100',
        //   style: TextStyle(
        //       color: color, fontSize: 16, fontWeight: FontWeight.bold),
        // ),
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.all(8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w800,
              color: color,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final fullWidth = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () {
        Navigator.pushNamed(
          context,
          ElevatorDetailsScreen.routeName,
          arguments: this.elevator,
        );
      },
      child: Card(
        elevation: 5,
        child: Container(
          height: 120,
          child: Row(
            children: <Widget>[
              FutureBuilder(
                  future: elevator.image,
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) {
                    return Container(
                      width: 100.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(4),
                          topLeft: Radius.circular(4),
                        ),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(snapshot.data),
                        ),
                      ),
                    );
                  }),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        elevator.name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 15,
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 4),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                elevator.address ?? '',
                                style:
                                    TextStyle(fontSize: 12, color: Colors.grey),
                              ),
                              Text(
                                elevator.responsable ?? '',
                                style:
                                    TextStyle(fontSize: 12, color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          _buildButtonColumn(Color.fromRGBO(187, 0, 0, 0.6),
                              Icons.radio_button_unchecked, '6'),
                          _buildButtonColumn(Color.fromRGBO(231, 140, 7, 0.6),
                              Icons.play_circle_outline, '8'),
                          _buildButtonColumn(Color.fromRGBO(43, 125, 43, 0.6),
                              Icons.check_circle_outline, '11')
                          // FlatButton.icon(
                          //     onPressed: () {},
                          //     icon: Icon(Icons.radio_button_unchecked,
                          //         color: Color.fromRGBO(187, 0, 0, 0.6)),
                          //     label: Text(elevator.pending.toString())),
                          // FlatButton.icon(
                          //     onPressed: () {},
                          //     icon: Icon(Icons.play_circle_outline,
                          //         color: Color.fromRGBO(231, 140, 7, 0.6)),
                          //     label: Text(elevator.inprocess.toString())),
                          // FlatButton.icon(
                          //     onPressed: () {},
                          //     icon: Icon(Icons.check_circle_outline,
                          //         color: Color.fromRGBO(43, 125, 43, 0.6)),
                          //     label: Text(elevator.complete.toString())),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
