import 'package:flutter/material.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/widget/silo_card.dart';

class SiloList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: new ListView.builder(
      itemCount: 3,
      itemBuilder: (context, i) {
        return new ExpansionTile(
            title: new Text("Корпус 2 (50)",
                style: new TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold
                )),
            children: <Widget>[
              new Column(
                children: <Widget>[
                 SiloCard(),
                 SiloCard(),
                 SiloCard()
                ],
              ),
            ]);
      },
    ));
  }
}
