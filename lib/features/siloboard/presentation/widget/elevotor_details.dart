import 'package:flutter/material.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/widget/silo_card.dart';

class ElevatorDetails extends StatelessWidget {
  Widget _buildTitleImage() {
    return Image.asset(
      'assets/images/001.jpg',
      width: 600,
      height: 200,
      fit: BoxFit.cover,
    );
  }

  Widget _buildStatusbar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 5,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FlatButton.icon(
                onPressed: () {},
                icon: Icon(Icons.radio_button_unchecked,
                    color: Color.fromRGBO(187, 0, 0, 0.6)),
                label: Text("5")),
            FlatButton.icon(
                onPressed: () {},
                icon: Icon(Icons.play_circle_outline,
                    color: Color.fromRGBO(231, 140, 7, 0.6)),
                label: Text("2")),
            FlatButton.icon(
                onPressed: () {},
                icon: Icon(Icons.check_circle_outline,
                    color: Color.fromRGBO(43, 125, 43, 0.6)),
                label: Text("1")),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            _buildTitleImage(),
            _buildStatusbar(),
            ExpansionTile(
              children: <Widget>[SiloCard()],
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Повышенная температура"),
                  Text(
                    "3",
                  )
                ],
              ),
            ),
            ExpansionTile(
              children: <Widget>[SiloCard(), SiloCard()],
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Пустые"),
                  Text(
                    "1",
                  )
                ],
              ),
            ),
            ExpansionTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Требуются замеры"),
                  Text(
                    "8",
                  )
                ],
              ),
              children: <Widget>[
                SiloCard(),
                // ListView.builder(
                //   itemCount: 3,
                //   itemBuilder: (context, i) {
                //     return SiloCard();
                //   },
                // )
              ],
            )
          ],
        ),
      ),
    );
  }
}
