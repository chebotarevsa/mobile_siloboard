import 'package:flutter/material.dart';

class SiloCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      //elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 65,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 60,
                width: 60,
                child: CircleAvatar(
                  backgroundColor: Colors.indigoAccent.withOpacity(0.5),
                  child: Text("1000"),
                  foregroundColor: Colors.white,
                )
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Пшеница озимая",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: 50,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.teal),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Center(
                            child: Text(
                          "Влажная",
                          style: TextStyle(color: Colors.teal, fontSize: 10),
                        )),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          //Icon(SAPIcon.increaseLineHeight, size: 15, color: Colors.blue.withOpacity(0.7)),
                          Icon(Icons.arrow_upward, color: Colors.grey, size: 18,),
                          Text("4м"),
                          VerticalDivider(),
                          Icon(Icons.get_app, color: Colors.grey,size: 18,),
                          Text("10т"),
                          VerticalDivider(),
                          Icon(Icons.ac_unit, color: Colors.grey,size: 18,),
                          Text("30ºC"),
                          VerticalDivider(),
                           Icon(Icons.filter_4, color: Colors.grey,size: 18,),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
