import 'package:flutter/material.dart';

showErrorDialog(context, text) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      content: Text(text),
      backgroundColor: Colors.red,
    ),
  );
}


showSuccessrDialog(context, text) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      content: Text(text),
      backgroundColor: Colors.green,
    ),
  );
}
