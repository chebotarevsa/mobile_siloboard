import 'package:equatable/equatable.dart';

class ElevatorListEvent extends Equatable{
  @override
  List<Object> get props => null;
}

class ElevatorListEventLoad extends ElevatorListEvent {}
class ElevatorListEventRefresh extends ElevatorListEvent {}