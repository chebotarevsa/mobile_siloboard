import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/siloboard/domain/repositories/siloboard_repository.dart';
import 'package:mobile_siloboard/features/siloboard/domain/usecase/find_all_elevator.dart';
import 'package:mobile_siloboard/features/siloboard/domain/usecase/usecase.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/models/ElevatorViewModel.dart';

import 'event.dart';
import 'state.dart';

class ElevatorListBloc extends Bloc<ElevatorListEvent, ElevatorListState> {
  final SiloboardRepository _siloboardRepository;
  final FindAllElevatorUseCase _findAllElevatorUseCase;

  ElevatorListBloc({
    @required siloboardRepository,
  })  : assert(siloboardRepository != null),
        this._siloboardRepository = siloboardRepository,
        this._findAllElevatorUseCase =
            FindAllElevatorUseCase(siloboardRepository: siloboardRepository);

  @override
  ElevatorListState get initialState => ElevatorListState.init();

  @override
  Stream<ElevatorListState> mapEventToState(ElevatorListEvent event) async* {
    if (event is ElevatorListEventLoad) {
      yield* _loadElevatos();
    }

    if (event is ElevatorListEventRefresh) {
      yield* _loadElevatos();
    }
  }

  Stream<ElevatorListState> _loadElevatos() async* {
    yield state.copyWith(loadElevatorStart: true);
    final result = await _findAllElevatorUseCase.call(NoParams());
    yield result.fold(
      (failure) => state.copyWith(
          loadElevatorFailed: true,
          loadElevatorFailedMessage: failure.message,
          loadElevatorStart: false),
      (elevators) => state.copyWith(
          elevators: elevators
              .map(
                (elevator) => ElevatorViewModel(
                  id: elevator.id,
                  name: elevator.name,
                  image: elevator.image,
                ),
              )
              .toList(),
          loadElevatorSuccess: true,
          loadElevatorStart: false),
    );

    yield state.copyWith(
      loadElevatorStart: false,
      loadElevatorFailed: false,
      loadElevatorFailedMessage: '',
      loadElevatorSuccess: false,
    );
  }
}
