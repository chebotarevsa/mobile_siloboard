import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/siloboard/presentation/models/ElevatorViewModel.dart';

class ElevatorListState extends Equatable {
  final List<ElevatorViewModel> elevators;
  final bool loadElevatorStart;
  final bool loadElevatorSuccess;
  final bool loadElevatorFailed;
  final String loadElevatorFailedMessage;

  ElevatorListState({
    @required this.elevators,
    @required this.loadElevatorStart,
    @required this.loadElevatorSuccess,
    @required this.loadElevatorFailed,
    @required this.loadElevatorFailedMessage,
  });

  factory ElevatorListState.init() {
    return ElevatorListState(
      elevators: [],
      loadElevatorFailed: false,
      loadElevatorFailedMessage: '',
      loadElevatorStart: false,
      loadElevatorSuccess: false,
    );
  }

  ElevatorListState copyWith({
    List<ElevatorViewModel> elevators,
    bool loadElevatorStart,
    bool loadElevatorSuccess,
    bool loadElevatorFailed,
    String loadElevatorFailedMessage,
  }) {
    return ElevatorListState(
      elevators: elevators ?? this.elevators,
      loadElevatorFailed: loadElevatorFailed ?? this.loadElevatorFailed,
      loadElevatorFailedMessage:
          loadElevatorFailedMessage ?? this.loadElevatorFailedMessage,
      loadElevatorStart: loadElevatorStart ?? this.loadElevatorStart,
      loadElevatorSuccess: loadElevatorSuccess ?? this.loadElevatorSuccess,
    );
  }

  @override
  List<Object> get props => [
        elevators,
        loadElevatorFailed,
        loadElevatorFailedMessage,
        loadElevatorStart,
        loadElevatorSuccess,
      ];
}
