import 'package:flutter/material.dart';

class ElevatorViewModel {
  final String id;
  final String name;
  final String address;
  final String responsable;
  final Future<String> image;
  final bool favorit;
  final int pending;
  final int inprocess;
  final int complete;

  ElevatorViewModel({
    this.address,
    this.responsable, 
    this.id,
    this.name,
    this.image,
    this.favorit,
    this.pending,
    this.inprocess,
    this.complete,
  });
}
