import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ElevatorModel extends Equatable {
  final String id;
  final String name;
  final DateTime createAt;
  final DateTime changeAt;
  final String createBy;
  final String changeBy;
  final String etag;

  ElevatorModel({
    @required this.id,
    @required this.name,
    @required this.createAt,
    @required this.changeAt,
    @required this.createBy,
    @required this.changeBy,
    @required this.etag,
  });

  factory ElevatorModel.fromJson(Map<String, dynamic> jsonMap) {
    return ElevatorModel(
        id: jsonMap['Id'],
        name: jsonMap['Name'],
        changeAt: jsonMap['ChangeAt'],
        changeBy: jsonMap['ChangeBy'],
        createAt: jsonMap['CreateAt'],
        createBy: jsonMap['CreateBy'],
        etag: jsonMap['Etag']);
  }

  Map<String, dynamic> toJson() {
    return {
      'Id': this.id,
      'Name': this.name,
      'ChangeAt': this.changeAt,
      'ChangeBy': this.changeBy,
      'CreateAt': this.createAt,
      'CreateBy': this.createBy,
      'Etag': this.etag
    };
  }

  @override
  List<Object> get props => [
        id,
        name,
        createAt,
        changeBy,
        createAt,
        createBy,
        etag,
      ];
}
