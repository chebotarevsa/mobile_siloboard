import 'package:equatable/equatable.dart';

class SiloModel extends Equatable {
  final String id;
  final String name;
  final String elevatorid;
  final String body;
  final int positionX;
  final int positionY;
  final DateTime createAt;
  final DateTime changeAt;
  final String createBy;
  final String changeBy;
  final String etag;

  SiloModel({
    this.id,
    this.name,
    this.elevatorid,
    this.body,
    this.positionX,
    this.positionY,
    this.createAt,
    this.changeAt,
    this.createBy,
    this.changeBy,
    this.etag,
  });

  factory SiloModel.fromJson(Map<String, dynamic> jsonMap) {
    return SiloModel(
        id: jsonMap['Id'],
        name: jsonMap['Name'],
        elevatorid: jsonMap['Elevatorid'],
        body: jsonMap['Body'],
        positionX: int.parse(jsonMap['PositionX'] ?? '0'),
        positionY: int.parse(jsonMap['PositionY'] ?? '0'),
        changeAt: jsonMap['ChangeAt'],
        changeBy: jsonMap['ChangeBy'],
        createAt: jsonMap['CreateAt'],
        createBy: jsonMap['CreateBy'],
        etag: jsonMap['Etag']);
  }

  Map<String, dynamic> toJson() {
    return {
      'Id': this.id,
      'Name': this.name,
      'PositionX': this.positionX,
      'PositionY': this.positionY,
      'Elevatorid': this.elevatorid,
      'Body': this.body,
      'ChangeAt': this.changeAt,
      'ChangeBy': this.changeBy,
      'CreateAt': this.createAt,
      'CreateBy': this.createBy,
      'Etag': this.etag
    };
  }

  @override
  List<Object> get props => [
        id,
        name,
        positionX,
        positionY,
        elevatorid,
        body,
        changeAt,
        changeBy,
        createAt,
        createBy,
        etag,
      ];
}
