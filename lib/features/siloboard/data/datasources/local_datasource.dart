import 'dart:convert';

import 'package:mobile_siloboard/features/siloboard/data/models/elevator_model.dart';
import 'package:shared_preferences/shared_preferences.dart';


enum _KeySore {
  ELEVATOR
}

abstract class LocalDataSource {
  Future<List<ElevatorModel>> findAllElevators();
}

class LocalDataSourceImpl implements LocalDataSource {
  final SharedPreferences sharedPreferences;

  LocalDataSourceImpl(this.sharedPreferences);

  @override
  Future<List<ElevatorModel>> findAllElevators() async {
    final jsonStr = sharedPreferences.getString(_KeySore.ELEVATOR.toString());
    final list = jsonDecode(jsonStr) as List;
    return list.map<ElevatorModel>((el) => ElevatorModel.fromJson(el)).toList();
  }
}
