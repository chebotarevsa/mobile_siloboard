import 'package:mobile_siloboard/features/siloboard/data/models/elevator_model.dart';
import 'package:mobile_siloboard/features/siloboard/data/models/silo_model.dart';

abstract class RemoteDataSource {
  Future<List<ElevatorModel>> fetchAllElevators();
  Future<ElevatorModel> fetchElevatorById(String id);
  Future<List<SiloModel>> fetchSiloByElevatorId(String id);
  Future<SiloModel> fetchSiloById(String id);
}
