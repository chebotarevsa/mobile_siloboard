abstract class CacheDataSource{
  dynamic get(String key);
  bool contein(String key);
  void put(String key, dynamic value);
  void purge(String key,);
  void purgeAll(String key,);
}