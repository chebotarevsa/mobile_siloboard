
import 'package:flutter/material.dart';

abstract class ImageDataSource{
   Future<String> getImageByElevatorId(String elevatorId);
}