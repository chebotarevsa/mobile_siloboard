import 'package:flutter/services.dart';
import '../image_datasource.dart';

class AssetImageDataSourceImpl implements ImageDataSource{
  @override
  Future<String> getImageByElevatorId(String elevatorId) {
        String path = "assets/images/$elevatorId.jpg";
        return rootBundle.load(path).then((value) {
          return path;
        }).catchError((_) {
          return "assets/images/elevator.jpg";
        });
  }
}