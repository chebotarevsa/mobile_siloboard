import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/common/utils/odata_client/lib/service/filter.dart';
import 'package:mobile_siloboard/common/utils/smp_auth_helper.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/settings/domian/entity/application_settings.dart';
import 'package:mobile_siloboard/features/settings/domian/repository/application_settings_repository.dart';
import 'package:mobile_siloboard/features/siloboard/data/datasources/remote_datasource.dart';
import 'package:mobile_siloboard/features/siloboard/data/models/elevator_model.dart';
import 'package:mobile_siloboard/features/siloboard/data/models/silo_model.dart';

class _OdataEntities {
  static const String ELEVATOR = 'ElevatorSet';
  static const String SILO = 'SiloSet';
}

class ODataExeption implements Exception {
  ODataExeption();
  factory ODataExeption.build(int statusCode, Map<String, dynamic> errorBody) {
    return ODataExeption();
  }
}

class ODataRemoteDataSourceImpl implements RemoteDataSource {
  final LogonRepository _logonRepository;
  final ApplicationSettingsRepository _applicationSettingsRepository;

  ODataRemoteDataSourceImpl({
    @required LogonRepository logonRepository,
    @required ApplicationSettingsRepository applicationSettingsRepository,
  })  : assert(logonRepository != null),
        assert(applicationSettingsRepository != null),
        this._logonRepository = logonRepository,
        this._applicationSettingsRepository = applicationSettingsRepository;

  Future<Uri> _buildUrl(
    String entityName, {
    String id,
    Map<String, String> ids,
    Filter filter,
    List<String> expands,
  }) async {
    final ApplicationSettings connectionParams =
        await _applicationSettingsRepository.getApplicationSettings();

    String expandsStr;
    String filersSrt;

    if (expands != null) {
      expandsStr = expands.join(',');
    }

    return Uri(
      scheme: connectionParams.https ? 'https' : 'http',
      host: connectionParams.host,
      port: connectionParams.port,
      path: '${connectionParams.appId}/$entityName',
      queryParameters: {r'$expand': expandsStr, r'$filter': filter.toString()},
    );
  }

  Map<String, String> _buildHeaders(Registration registration) {
    final authHeader =
        SMPAuthHelper.getSMPAuthHeaders(registration: registration);
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
    headers.addAll(authHeader);
    return headers;
  }

  @override
  Future<List<ElevatorModel>> fetchAllElevators() async {
    http.Response response = await http.get(
        await _buildUrl(_OdataEntities.ELEVATOR),
        headers: _buildHeaders(_logonRepository.registration));
    Map<String, dynamic> jsonResult = json.decode(response.body);
    if (response.statusCode != 200) {
      throw ODataExeption.build(response.statusCode, jsonResult);
    }
    List result = jsonResult["d"]["results"] as List;
    return result
        .map<ElevatorModel>((el) => ElevatorModel.fromJson(el))
        .toList();
  }

  @override
  Future<ElevatorModel> fetchElevatorById(String id) async {
    http.Response response = await http.get(
        await _buildUrl(_OdataEntities.ELEVATOR, id: id),
        headers: _buildHeaders(_logonRepository.registration));
    Map<String, dynamic> jsonResult = json.decode(response.body);
    Map<String, dynamic> result = jsonResult["d"];
    return ElevatorModel.fromJson(result);
  }

  @override
  Future<List<SiloModel>> fetchSiloByElevatorId(String id) async {
    http.Response response = await http.get(
        await _buildUrl(_OdataEntities.SILO,
            filter: FilterLogic.eq(
                FilterProperty.name('Elevatorid'), FilterValue.value(id))),
        headers: _buildHeaders(_logonRepository.registration));
    Map<String, dynamic> jsonResult = json.decode(response.body);
    List result = jsonResult["d"]["results"] as List;
    return result.map<SiloModel>((el) => SiloModel.fromJson(el)).toList();
  }

  @override
  Future<SiloModel> fetchSiloById(String id) async {
    http.Response response = await http.get(
        await _buildUrl(_OdataEntities.SILO, id: id),
        headers: _buildHeaders(_logonRepository.registration));
    Map<String, dynamic> jsonResult = json.decode(response.body);
    Map<String, dynamic> result = jsonResult["d"];
    return SiloModel.fromJson(result);
  }
}
