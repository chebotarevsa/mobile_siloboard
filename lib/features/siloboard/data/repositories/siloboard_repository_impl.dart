import 'package:dartz/dartz.dart';
import 'package:mobile_siloboard/common/result/failure.dart';
import 'package:mobile_siloboard/features/siloboard/data/datasources/cache_datasource.dart';
import 'package:mobile_siloboard/features/siloboard/data/datasources/image_datasource.dart';
import 'package:mobile_siloboard/features/siloboard/data/datasources/remote_datasource.dart';
import 'package:mobile_siloboard/features/siloboard/domain/entities/elevator.dart';
import 'package:mobile_siloboard/features/siloboard/domain/entities/silo.dart';
import 'package:mobile_siloboard/features/siloboard/domain/repositories/siloboard_repository.dart';

class SiloboardRepositoryImpl implements SiloboardRepository {
  RemoteDataSource _remoteDataSource;
  ImageDataSource _imageDataSource;
  CacheDataSource _cacheDataSource;

  SiloboardRepositoryImpl(
      {RemoteDataSource remoteDataSource,
      ImageDataSource imageDataSource,
      CacheDataSource cacheDataSource})
      : _remoteDataSource = remoteDataSource,
        _imageDataSource = imageDataSource,
        _cacheDataSource = cacheDataSource;

  @override
  Future<Either<Failure, List<Elevator>>> findAllElevator() async {
    try {
      final elevatorModels = await _remoteDataSource.fetchAllElevators();
      final elevators = elevatorModels
          .map<Elevator>((model) => Elevator(
              id: model.id,
              name: model.name,
              image: _imageDataSource.getImageByElevatorId(model.id)))
          .toList();
      return Right(elevators);
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }

  @override
  Future<Either<Failure, Elevator>> findElevatorById(String id) async {
    try {
      final model = await _remoteDataSource.fetchElevatorById(id);
      final elevator = Elevator(
          id: model.id,
          name: model.name,
          image: _imageDataSource.getImageByElevatorId(model.id));
      return Right(elevator);
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }

  @override
  Future<Either<Failure, Silo>> findSiloById(String id) async {
    try {
      final model = await _remoteDataSource.fetchSiloById(id);
      final silo = Silo(
          id: model.id,
          name: model.name,
          body: model.body,
          elevatorid: model.elevatorid,
          positionX: model.positionX,
          positionY: model.positionY);
      return Right(silo);
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }

  @override
  Future<Either<Failure, List<Silo>>> findSilosByElevatorId(String id) async {
    try {
      final siloModel = await _remoteDataSource.fetchSiloByElevatorId(id);
      final silos = siloModel
          .map<Silo>((model) => Silo(
              id: model.id,
              name: model.name,
              body: model.body,
              elevatorid: model.elevatorid,
              positionX: model.positionX,
              positionY: model.positionY))
          .toList();
      return Right(silos);
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }
}
