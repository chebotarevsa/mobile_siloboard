import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mobile_siloboard/common/result/failure.dart';

abstract class UseCase<Result, Params> {
  Future<Either<Failure, Result>> call(Params params);
}

class NoParams extends Equatable {
  @override
  List<Object> get props => [];
}