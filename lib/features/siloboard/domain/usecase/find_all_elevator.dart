import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/common/result/failure.dart';
import 'package:mobile_siloboard/features/siloboard/domain/entities/elevator.dart';
import 'package:mobile_siloboard/features/siloboard/domain/repositories/siloboard_repository.dart';
import 'package:mobile_siloboard/features/siloboard/domain/usecase/usecase.dart';

class FindAllElevatorUseCase extends UseCase<List<Elevator>, NoParams> {
  final SiloboardRepository _siloboardRepository;

  FindAllElevatorUseCase({
    @required siloboardRepository,
  })  : assert(siloboardRepository != null),
        this._siloboardRepository = siloboardRepository;

  @override
  Future<Either<Failure, List<Elevator>>> call(NoParams params) async {
    return await _siloboardRepository.findAllElevator();
  }
}
