import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/siloboard/domain/entities/silo.dart';

class Elevator extends Equatable {
  final String id;
  final String name;
  final List<Silo> silos;
  final Future<String> image;

  Elevator({
    @required this.id, 
    @required this.name, 
    this.image,
    this.silos, 
  });

  @override
  List<Object> get props => [id, name, image, silos ];
}
