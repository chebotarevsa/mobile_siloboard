import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Bank extends Equatable {
  final String id;
  final String name;

  Bank({
    @required this.id,
    @required this.name,
  });

  @override
  List<Object> get props => [id, name];
}
