import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/siloboard/domain/entities/bank.dart';

class Silo extends Equatable {
  final String id;
  final String name;
  final String elevatorid;
  final String body;
  final int positionX;
  final int positionY;
  //final List<Bank> banks;

  Silo({
    @required this.id,
    @required this.name,
    @required this.elevatorid,
    @required this.body,
    @required this.positionX,
    @required this.positionY
  });

  @override
  List<Object> get props => [ id, name, elevatorid, body, positionX, positionY];
}
