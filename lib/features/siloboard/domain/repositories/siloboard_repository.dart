import 'package:dartz/dartz.dart';

import '../../../../common/result/failure.dart';
import '../entities/elevator.dart';
import '../entities/silo.dart';

abstract class SiloboardRepository {
  Future<Either<Failure, List<Elevator>>> findAllElevator();
  Future<Either<Failure, Elevator>> findElevatorById(String id);
  Future<Either<Failure, List<Silo>>> findSilosByElevatorId(String id);
  Future<Either<Failure, Silo>> findSiloById(String id);
}
