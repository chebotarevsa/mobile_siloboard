import 'package:flutter/material.dart';

class LogonTemplate extends StatelessWidget {
  final Widget background;
  final Widget foreground;

  const LogonTemplate({Key key, this.background, this.foreground})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      top: true,
      child: Stack(
        children: <Widget>[
          if (background != null) background,
          if (foreground != null) foreground,
        ],
      ),
    );
  }
}
