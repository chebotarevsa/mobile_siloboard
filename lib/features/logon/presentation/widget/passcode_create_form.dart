import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/bloc.dart';
import 'package:mobile_siloboard/l10n/localization.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/dialog.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/logon_busy.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/logon_form.dart';

class PasscodeCreateForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PasscodeCreateFormFormState();
}

class _PasscodeCreateFormFormState extends State<PasscodeCreateForm> {
  final _passcodeController1 = TextEditingController();
  final _passcodeController2 = TextEditingController();
  final _passcodeFocus1 = FocusNode();
  final _passcodeFocus2 = FocusNode();
  final _formKey = GlobalKey<FormState>();
  PasscodeCreateBloc _passcodeCreateBloc;

  @override
  void initState() {
    super.initState();
    _passcodeCreateBloc = BlocProvider.of<PasscodeCreateBloc>(context);
    _passcodeController1.addListener(_onPasscode1Changed);
    _passcodeController2.addListener(_onPasscode2Changed);
  }

  @override
  void dispose() {
    _passcodeController1.dispose();
    _passcodeController2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) =>
      BlocListener<PasscodeCreateBloc, PasscodeCreateState>(
        listener: this._listener,
        child: LogonFrom(
          formKey: _formKey,
          leftButton: _buildCreateButton(),
          rightButton: _buildCancelButton(),
          fields: <Widget>[
            _buildPasscode1Field(),
            _buildPasscode2Field(),
          ],
          busy: _buildBusy(),
        ),
      );

  void _onPasscode1Changed() {
    _passcodeCreateBloc.add(
      PasscodeCreateChanged1(passcode1: _passcodeController1.text),
    );
  }

  void _onPasscode2Changed() {
    _passcodeCreateBloc.add(
      PasscodeCreateChanged2(passcode2: _passcodeController2.text),
    );
  }

  void _onCreatePassCodeButtonPressed() {
    if (_formKey.currentState.validate()) {
      _passcodeCreateBloc.add(
        PasscodeCreateSubmit(
            passcode1: _passcodeController1.text,
            passcode2: _passcodeController2.text),
      );
    }
  }

  void _onCancelPasscodeButtonPressed() {
    _passcodeCreateBloc.add(
      PasscodeCreateCancel(),
    );
  }

  void _shownCancelPasscodeDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(AppLocalization.of(context).logonTitleCancelOfRegistration()),
        content: Text(AppLocalization.of(context).logonTextCancelOfRegistration()),
        actions: <Widget>[
          FlatButton(
            child: Text(
              AppLocalization.of(context).logonTextYes(),
              style: TextStyle(
                color: Colors.red,
                decoration: TextDecoration.underline,
              ),
            ),
            onPressed: _onCancelPasscodeButtonPressed,
          ),
          FlatButton(
            child: Text(
              AppLocalization.of(context).logonTextNo(),
              style: TextStyle(
                color: Colors.blue,
                decoration: TextDecoration.underline,
              ),
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      ),
    );
  }

  void _listener(BuildContext context, PasscodeCreateState state) {
    if (state.createPasscodeSuccess)
      Navigator.of(context).pushReplacementNamed('/silobiard');

    if (state.unregisterFailure) {
      showErrorDialog(context, state.errorMessageUnregister);
    }

    if (state.unregisterSuccess || state.unregisterFailure)
      Navigator.of(context).pushReplacementNamed('/');
  }

  Widget _buildBusy() => BlocBuilder<PasscodeCreateBloc, PasscodeCreateState>(
        builder: (context, state) =>
            state.createPasscodeStart ? LogonBusy() : Container(),
      );

  Widget _buildCancelButton() => FlatButton(
        child: Text(
          AppLocalization.of(context).logonTextCancel(),
          style: TextStyle(
            color: Colors.blue.withOpacity(0.7),
            decoration: TextDecoration.underline,
          ),
        ),
        onPressed: _shownCancelPasscodeDialog,
      );

  Widget _buildCreateButton() =>
      BlocBuilder<PasscodeCreateBloc, PasscodeCreateState>(
        builder: (context, state) => RaisedButton(
          onPressed:
              state.createPasscodeStart ? null : _onCreatePassCodeButtonPressed,
          color: Colors.green,
          child: Text(AppLocalization.of(context).logonTextSet()),
        ),
      );

  Widget _buildPasscode1Field() =>
      BlocBuilder<PasscodeCreateBloc, PasscodeCreateState>(
        builder: (context, state) => TextFormField(
            decoration: InputDecoration(
              labelText: AppLocalization.of(context).logonLabelPasscode(),
              hintText: AppLocalization.of(context).logonHintSetPasscode(),
              icon: Icon(Icons.vpn_key),
            ),
            focusNode: _passcodeFocus1,
            controller: _passcodeController1,
            obscureText: true,
            autovalidate: state.isAutoValidatePasscode1,
            validator: (value) => state.isValidPasscode1
                ? null
                : state.errorMassageValidatePasscode1),
      );

  Widget _buildPasscode2Field() =>
      BlocBuilder<PasscodeCreateBloc, PasscodeCreateState>(
        builder: (context, state) => TextFormField(
            decoration: InputDecoration(
              labelText: AppLocalization.of(context).logonLabelPasscodeRepeat(),
              hintText: AppLocalization.of(context).logonHintRepeatPasscode(),
              icon: Icon(Icons.vpn_key),
            ),
            controller: _passcodeController2,
            focusNode: _passcodeFocus2,
            autovalidate: state.isAutoValidatePasscode2,
            obscureText: true,
            validator: (value) => state.isValidPasscode2
                ? null
                : state.errorMassageValidatePasscode2),
      );
}
