import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info/package_info.dart';

class Background extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final verion = RepositoryProvider.of<PackageInfo>(context).version;
    return Container(
      constraints: BoxConstraints.expand(),
      decoration: BoxDecoration(
        color: Colors.grey,
        image: DecorationImage(
          image: AssetImage("assets/images/background.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Align(
          alignment: Alignment.bottomRight,
          child: new Text(verion,  style: TextStyle(color: Colors.white.withOpacity(0.6)),),
        ),
      ),
    );
  }
}
