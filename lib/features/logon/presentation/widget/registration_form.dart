import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/bloc.dart';
import 'package:mobile_siloboard/l10n/localization.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/dialog.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/logon_busy.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/logon_form.dart';

class RegistrationForm extends StatefulWidget {
  @override
  State<RegistrationForm> createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _passwordFocus = FocusNode();
  final _usernameFocus = FocusNode();
  final _formKey = GlobalKey<FormState>();
  RegistrationBloc _registrationBloc;

  @override
  void initState() {
    super.initState();
    _registrationBloc = BlocProvider.of<RegistrationBloc>(context);
    _usernameController.addListener(_onUsernameChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegistrationBloc, RegistrationState>(
      listener: this._listener,
      child: LogonFrom(
        formKey: _formKey,
        leftButton: _buildRegistrationButton(),
        rightButton: _buildSettingsButton(context),
        fields: <Widget>[
          _buildUsernameField(),
          _buildPasswordField(),
        ],
        busy: _buildBusy(),
      ),
    );
  }

  _onLoginButtonPressed() {
    if (_formKey.currentState.validate()) {
      _registrationBloc.add(
        RegistrationSubmit(
          username: _usernameController.text,
          password: _passwordController.text,
        ),
      );
    }
  }

  _changeSettings() {
    Navigator.of(context).pushNamed('/settings');
  }

  void _onUsernameChanged() {
    _registrationBloc
        .add(RegistrationUsernameChanged(username: _usernameController.text));
  }

  void _onPasswordChanged() {
    _registrationBloc
        .add(RegistrationPasswordChanged(password: _passwordController.text));
  }

  void _listener(BuildContext context, RegistrationState state) {
    if (state.registrationSuccess)
      Navigator.of(context).pushReplacementNamed('/logon/create_passcode');

    if (state.registrationFailure)
      showErrorDialog(context, state.registrationErrorMessage);
  }

  Widget _buildBusy() => BlocBuilder<RegistrationBloc, RegistrationState>(
        builder: (context, state) =>
            state.registrationStart ? LogonBusy() : Container(),
      );

  Widget _buildSettingsButton(BuildContext context) => FlatButton(
        child: Text(
          AppLocalization.of(context).logonTextSettings(),
          style: TextStyle(
            color: Colors.blue.withOpacity(0.7),
            decoration: TextDecoration.underline,
          ),
        ),
        onPressed: _changeSettings,
      );

  Widget _buildRegistrationButton() =>
      BlocBuilder<RegistrationBloc, RegistrationState>(
        builder: (context, state) => RaisedButton(
          onPressed: !state.registrationStart ? _onLoginButtonPressed : null,
          color: Colors.green,
          child: Text(AppLocalization.of(context).logonTextRergistration()),
        ),
      );

  Widget _buildPasswordField() =>
      BlocBuilder<RegistrationBloc, RegistrationState>(
        builder: (context, state) => TextFormField(
          decoration: InputDecoration(
            labelText: AppLocalization.of(context).logonLabelPassword(),
            icon: Icon(Icons.vpn_key),
          ),
          controller: _passwordController,
          obscureText: true,
          focusNode: _passwordFocus,
          autovalidate: state.isAutoValidatePassword,
          validator: (_) =>
              state.isValidPassword ? null : state.errorMessageValidPassword,
        ),
      );

  Widget _buildUsernameField() =>
      BlocBuilder<RegistrationBloc, RegistrationState>(
        builder: (context, state) => TextFormField(
            decoration: InputDecoration(
              labelText: AppLocalization.of(context).logonLabelUsername(),
              icon: Icon(Icons.person),
            ),
            controller: _usernameController,
            focusNode: _usernameFocus,
            autovalidate: state.isAutoValidateUsername,
            validator: (value) => state.isValidUsername
                ? null
                : state.errorMessageValidateUsername),
      );
}
