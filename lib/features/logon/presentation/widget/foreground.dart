import 'package:flutter/material.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/logo.dart';
import 'package:mobile_siloboard/l10n/localization.dart';

class Foreground extends StatelessWidget {
  final Widget child;

  const Foreground({Key key, this.child})
      : assert(child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: new Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (MediaQuery.of(context).orientation == Orientation.portrait)
              Logo(),
            Padding(
              padding: const EdgeInsets.all(14.0),
              child: Text(
                AppLocalization.of(context).title(),
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
              ),
            ),
            child,
          ],
        ),
      ),
    );
  }
}
