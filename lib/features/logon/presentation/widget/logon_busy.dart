import 'package:flutter/material.dart';

class LogonBusy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.6),
            borderRadius: BorderRadius.all(Radius.circular(4))),
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
