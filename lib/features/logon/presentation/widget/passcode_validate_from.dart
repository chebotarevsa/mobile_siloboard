import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/bloc.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/dialog.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/logon_form.dart';
import 'package:mobile_siloboard/l10n/localization.dart';

import 'logon_busy.dart';

class PasscodeValidateForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PasscodeValidateFormFormState();
  }
}

class _PasscodeValidateFormFormState extends State<PasscodeValidateForm> {
  final _passcodeController = TextEditingController();
  PasscodeValidateBloc _passcodeValidateBloc;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _passcodeValidateBloc = BlocProvider.of<PasscodeValidateBloc>(context);
    _passcodeController.addListener(_onPasscodeChange);
  }

  void _onValidatePassCodeButtonPressed() {
    if (_formKey.currentState.validate()) {
      _passcodeValidateBloc
          .add(PasscodeValidateSubmit(passcode: _passcodeController.text));
    }
  }

  void _onForgetPasscodeButtonPressed() {
    _passcodeValidateBloc.add(PasscodeValidateForget());
  }

  void _showForgetPasscodeDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(AppLocalization.of(context).logonTitleResetPasscode()),
        content: Text(AppLocalization.of(context).logonTextResetPasscode()),
        actions: <Widget>[
          FlatButton(
            child: Text(
              AppLocalization.of(context).logonTextContinue(),
              style: TextStyle(color: Colors.red),
            ),
            onPressed: _onForgetPasscodeButtonPressed,
          ),
          FlatButton(
            child: Text(
              AppLocalization.of(context).logonTextCancel(),
              style: TextStyle(color: Colors.blue),
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      ),
    );
  }

  void _onPasscodeChange() {
    _passcodeValidateBloc
        .add(PasscodeValidateChanged(passcode: _passcodeController.text));
  }

  void _listener(BuildContext context, PasscodeValidateState state) {
    if (state.validatePasscodeSuccess) {
      Navigator.of(context).pushReplacementNamed('/silobiard');
    }
    if (state.validatePasscodeFailure) {
      showErrorDialog(
          context, AppLocalization.of(context).logonMessageInvalidPasscode());
    }
    if (state.removeRegistrationSuccess) {
      Navigator.of(context).pushReplacementNamed('/');
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PasscodeValidateBloc, PasscodeValidateState>(
      listener: this._listener,
      child: BlocBuilder<PasscodeValidateBloc, PasscodeValidateState>(
        builder: (context, state) {
          return LogonFrom(
            formKey: _formKey,
            leftButton: _buildEnterButton(),
            rightButton: _buildForgotButton(),
            fields: <Widget>[_buildPasscodeField()],
            busy: _buildBusy(),
          );
        },
      ),
    );
  }

  Widget _buildBusy() {
    return BlocBuilder<PasscodeValidateBloc, PasscodeValidateState>(
        builder: (context, state) {
      return state.validatePasscodeStart || state.removeRegistrationStart
          ? LogonBusy()
          : Container();
    });
  }

  Widget _buildForgotButton() {
    return FlatButton(
      child: Text(
        AppLocalization.of(context).logonTextForgotPasscode(),
        style: TextStyle(
          color: Colors.blue.withOpacity(0.7),
          decoration: TextDecoration.underline,
        ),
      ),
      onPressed: _showForgetPasscodeDialog,
    );
  }

  Widget _buildEnterButton() {
    return BlocBuilder<PasscodeValidateBloc, PasscodeValidateState>(
        builder: (context, state) {
      return RaisedButton(
        onPressed: !state.validatePasscodeStart
            ? _onValidatePassCodeButtonPressed
            : null,
        color: Colors.green,
        child: Text(AppLocalization.of(context).logonTextEnter()),
      );
    });
  }

  Widget _buildPasscodeField() {
    return BlocBuilder<PasscodeValidateBloc, PasscodeValidateState>(
        builder: (context, state) {
      return TextFormField(
          decoration: InputDecoration(
            labelText: AppLocalization.of(context).logonLabelPassword(),
            hintText: AppLocalization.of(context).logonHintEnterPasscode(),
            icon: Icon(Icons.vpn_key),
          ),
          autovalidate: state.isAutoValidatePasscode,
          controller: _passcodeController,
          obscureText: true,
          validator: (value) => state.isValidInputPasscode
              ? null
              : state.errorMessageValidateInputPasscode);
    });
  }
}
