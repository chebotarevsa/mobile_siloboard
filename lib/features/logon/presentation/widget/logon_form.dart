import 'dart:math';

import 'package:flutter/material.dart';

class LogonFrom extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final Widget leftButton;
  final Widget rightButton;
  final List<Widget> fields;
  final Widget busy;

  LogonFrom({
    @required this.formKey,
    @required this.leftButton,
    @required this.rightButton,
    @required this.fields,
    this.busy,
  })  : assert(formKey != null),
        assert(leftButton != null),
        assert(rightButton != null),
        assert(fields != null && fields is List<Widget>);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double formWidth = max(300, size.width / 1.4);
    final buttonWidth = formWidth / 2.5;
    return Container(
      width: formWidth,
      child: Center(
        child: Card(
          child: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      ...fields,
                      SizedBox(height: 16),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          SizedBox(
                            child: leftButton,
                            width: buttonWidth,
                          ),
                          SizedBox(
                            width: buttonWidth,
                            child: rightButton,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              busy ?? Container()
            ],
          ),
        ),
      ),
    );
  }
}
