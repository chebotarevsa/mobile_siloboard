import 'dart:math';

import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: max(MediaQuery.of(context).size.width * 0.3, 300),
      height: min(MediaQuery.of(context).size.height * 0.10, 200),
      decoration: BoxDecoration(
          image: DecorationImage(
        fit: BoxFit.contain,
        image: AssetImage("assets/images/logo.png"),
      )),
    );
  }
}
