import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/l10n/localization.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/background.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/foreground.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/logon_template.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/passcode_create_form.dart';

class PasscodeCreateScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final localization = AppLocalization.of(context);
    return BlocProvider<PasscodeCreateBloc>(
      create: (context) {
        return PasscodeCreateBloc(
          registrationRepository:
              RepositoryProvider.of<RegistrationRepository>(context),
          logonRepository: RepositoryProvider.of<LogonRepository>(context),
          appLocalization: localization,
        );
      },
      child: Scaffold(
        body: LogonTemplate(
          background: Background(),
          foreground: Foreground(
            child: PasscodeCreateForm(),
          ),
        ),
      ),
    );
  }
}
