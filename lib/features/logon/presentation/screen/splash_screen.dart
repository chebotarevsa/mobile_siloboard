import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/background.dart';
import 'package:mobile_siloboard/features/logon/presentation/widget/logon_template.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SplashBloc>(
      create: (context) {
        return SplashBloc(
          logonRepository: RepositoryProvider.of<LogonRepository>(context),
          registrationRepository:
              RepositoryProvider.of<RegistrationRepository>(context),
        )..add(
            SplashLogonStarted(),
          );
      },
      child: BlocListener<SplashBloc, SplashState>(
        listener: (context, state) {
          if (state == SplashState.LOGINED) {
            Navigator.of(context).pushReplacementNamed('/silobiard');
          }
          if (state == SplashState.UNREGISTRED) {
            Navigator.of(context).pushReplacementNamed('/logon/register');
          }
          if (state == SplashState.REGISTRED) {
            Navigator.of(context)
                .pushReplacementNamed('/logon/validate_passcode');
          }
        },
        child: Scaffold(
          body: LogonTemplate(
            background: Background(),
          ),
        ),
      ),
    );
  }
}
