import 'package:equatable/equatable.dart';

class Passcode extends Equatable {
  final String passcode;
  Passcode(this.passcode);

  @override
  List<Object> get props => [passcode];
}
