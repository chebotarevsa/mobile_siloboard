import 'package:flutter/cupertino.dart';

@immutable
class Credential {
  final String username;
  final String password;

  Credential({
    @required this.username,
    @required this.password,
  });
}
