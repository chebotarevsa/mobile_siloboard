import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
class Registration  extends Equatable {
  final String username;
  final String password;
  final String appCId;

  Registration({
    @required this.username,
    @required this.password,
    @required this.appCId,
  });

  @override
  List<Object> get props => [username, password, appCId];
}
