import 'package:equatable/equatable.dart';

class PasscodeValidateState extends Equatable {
  final String passcode;
  final bool isValidInputPasscode;
  final String errorMessageValidateInputPasscode;
  final bool validatePasscodeStart;
  final bool validatePasscodeSuccess;
  final bool validatePasscodeFailure;
  final String errorMessageValidatePasscode;
  final bool removeRegistrationStart;
  final bool removeRegistrationSuccess;
  final bool removeRegistrationFailure;
  final String errorMessageRemoveRegistration;
  final bool isAutoValidatePasscode;

  bool get isValidForm => isValidInputPasscode;

  PasscodeValidateState({
    this.passcode,
    this.isValidInputPasscode,
    this.errorMessageValidateInputPasscode,
    this.validatePasscodeStart,
    this.validatePasscodeSuccess,
    this.validatePasscodeFailure,
    this.errorMessageValidatePasscode,
    this.removeRegistrationStart,
    this.removeRegistrationSuccess,
    this.removeRegistrationFailure,
    this.errorMessageRemoveRegistration,
    this.isAutoValidatePasscode,
  });

  @override
  List<Object> get props => [
        passcode,
        isValidInputPasscode,
        errorMessageValidateInputPasscode,
        validatePasscodeStart,
        validatePasscodeSuccess,
        validatePasscodeFailure,
        errorMessageValidatePasscode,
        removeRegistrationStart,
        removeRegistrationSuccess,
        removeRegistrationFailure,
        errorMessageRemoveRegistration,
        isAutoValidatePasscode,
      ];

  factory PasscodeValidateState.init() {
    return PasscodeValidateState(
      errorMessageValidateInputPasscode: '',
      errorMessageValidatePasscode: '',
      isValidInputPasscode: false,
      passcode: '',
      validatePasscodeFailure: false,
      validatePasscodeStart: false,
      validatePasscodeSuccess: false,
      removeRegistrationStart: false,
      removeRegistrationSuccess: false,
      removeRegistrationFailure: false,
      errorMessageRemoveRegistration: '',
      isAutoValidatePasscode: false,
    );
  }

  PasscodeValidateState copyWith({
    String passcode,
    bool isValidInputPasscode,
    String errorMessageValidateInputPasscode,
    bool validatePasscodeStart,
    bool validatePasscodeSuccess,
    bool validatePasscodeFailure,
    String errorMessageValidatePasscode,
    bool removeRegistrationStart,
    bool removeRegistrationSuccess,
    bool removeRegistrationFailure,
    String errorMessageRemoveRegistration,
    bool isAutoValidatePasscode,
  }) {
    return PasscodeValidateState(
      errorMessageValidateInputPasscode: errorMessageValidateInputPasscode ??
          this.errorMessageValidateInputPasscode,
      errorMessageValidatePasscode: errorMessageValidateInputPasscode ??
          this.errorMessageValidateInputPasscode,
      isValidInputPasscode: isValidInputPasscode ?? this.isValidInputPasscode,
      passcode: passcode ?? this.passcode,
      validatePasscodeFailure:
          validatePasscodeFailure ?? this.validatePasscodeFailure,
      validatePasscodeStart:
          validatePasscodeStart ?? this.validatePasscodeStart,
      validatePasscodeSuccess:
          validatePasscodeSuccess ?? this.validatePasscodeSuccess,
      removeRegistrationStart:
          removeRegistrationStart ?? this.removeRegistrationStart,
      removeRegistrationFailure:
          removeRegistrationFailure ?? this.removeRegistrationFailure,
      removeRegistrationSuccess:
          removeRegistrationSuccess ?? this.removeRegistrationSuccess,
      errorMessageRemoveRegistration:
          errorMessageRemoveRegistration ?? this.errorMessageRemoveRegistration,
          isAutoValidatePasscode: isAutoValidatePasscode ?? this.isAutoValidatePasscode,
    );
  }

  @override
  String toString() {
    return 'passcode:$passcode, isValidInputPasscode:$isValidInputPasscode, errorMessageValidateInputPasscode:$errorMessageValidateInputPasscode, ' +
        'validatePasscodeStart:$validatePasscodeStart, validatePasscodeSuccess:$validatePasscodeSuccess, validatePasscodeFailure: $validatePasscodeFailure, ' +
        'errorMessageValidatePasscode:$errorMessageValidatePasscode, removeRegistrationStart:$removeRegistrationStart, removeRegistrationSuccess:$removeRegistrationSuccess, ' +
        'removeRegistrationFailure: $removeRegistrationFailure, errorMessageRemoveRegistration: $errorMessageRemoveRegistration, ' + 
        'isAutoValidatePasscode: $isAutoValidatePasscode';
  }
}
