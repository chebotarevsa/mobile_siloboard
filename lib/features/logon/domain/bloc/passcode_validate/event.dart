import 'package:equatable/equatable.dart';

abstract class PasscodeValidateEvent extends Equatable {
  const PasscodeValidateEvent();

  @override
  List<Object> get props => [];
}

class PasscodeValidateSubmit extends PasscodeValidateEvent {
  final String passcode;

  const PasscodeValidateSubmit({this.passcode});

  @override
  List<Object> get props => [passcode];
}

class PasscodeValidateChanged extends PasscodeValidateEvent {
  final String passcode;

  const PasscodeValidateChanged({this.passcode});

  @override
  List<Object> get props => [passcode   ];
}


class PasscodeValidateForget extends PasscodeValidateEvent {}

