import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc//bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/passcode_validate/event.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/passcode_validate/state.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/passcode.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/l10n/localization.dart';

class PasscodeValidateBloc
    extends Bloc<PasscodeValidateEvent, PasscodeValidateState> {
  static const String EMPTY = '';

  final RegistrationRepository registrationRepository;
  final LogonRepository logonRepository;
  final AppLocalization appLocalization;

  PasscodeValidateBloc({
    @required this.registrationRepository,
    @required this.logonRepository,
    @required this.appLocalization,
  })  : assert(registrationRepository != null),
        assert(appLocalization != null),
        assert(logonRepository != null);

  @override
  PasscodeValidateState get initialState =>
      PasscodeValidateState.init().copyWith(
        errorMessageValidateInputPasscode:
            appLocalization.logonMessagePasscodeEmpty(),
      );

  @override
  Stream<PasscodeValidateState> mapEventToState(event) async* {
    if (event is PasscodeValidateChanged) {
      yield* _validatePasscode(event.passcode);
    }

    if (event is PasscodeValidateSubmit) {
      yield* _submitPasscode(event.passcode);
    }

    if (event is PasscodeValidateForget) {
      yield* _passcodeForget();
    }
  }

  Stream<PasscodeValidateState> _validatePasscode(String passcode) async* {
    if (passcode.isEmpty) {
      yield state.copyWith(
          passcode: passcode,
          isValidInputPasscode: false,
          isAutoValidatePasscode: true,
          errorMessageValidateInputPasscode:
              appLocalization.logonMessagePasscodeEmpty());
    } else {
      yield state.copyWith(
        passcode: passcode,
        isValidInputPasscode: true,
        isAutoValidatePasscode: true,
        errorMessageValidateInputPasscode: EMPTY,
      );
    }
  }

  Stream<PasscodeValidateState> _passcodeForget() async* {
    yield state.copyWith(
        removeRegistrationFailure: false,
        removeRegistrationStart: true,
        removeRegistrationSuccess: false,
        errorMessageRemoveRegistration: EMPTY);
    logonRepository.clearRegistration();
    final result = await registrationRepository.removeRegistration();
    yield result.fold(
      (failure) => state.copyWith(
        removeRegistrationFailure: true,
        removeRegistrationStart: false,
        removeRegistrationSuccess: false,
        errorMessageRemoveRegistration: failure.message,
      ),
      (success) => state.copyWith(
        removeRegistrationFailure: false,
        removeRegistrationStart: false,
        removeRegistrationSuccess: true,
        errorMessageRemoveRegistration: EMPTY,
      ),
    );
    yield state.copyWith(
      removeRegistrationFailure: false,
      removeRegistrationStart: false,
      removeRegistrationSuccess: false,
      errorMessageRemoveRegistration: EMPTY,
    );
  }

  Stream<PasscodeValidateState> _submitPasscode(String passcode) async* {
    yield state.copyWith(
        validatePasscodeStart: true,
        validatePasscodeFailure: false,
        validatePasscodeSuccess: false);
    final result =
        await registrationRepository.restoreRegistration(Passcode(passcode));
    yield result.fold(
        (failure) => state.copyWith(
              validatePasscodeStart: false,
              validatePasscodeFailure: true,
              validatePasscodeSuccess: false,
              errorMessageValidatePasscode: failure.toString(),
            ), (registration) {
      logonRepository.registration = registration;
      return state.copyWith(
        validatePasscodeStart: false,
        validatePasscodeFailure: false,
        validatePasscodeSuccess: true,
      );
    });
    yield state.copyWith(
      validatePasscodeStart: false,
      validatePasscodeFailure: false,
      validatePasscodeSuccess: false,
      errorMessageValidatePasscode: EMPTY,
    );
  }
}
