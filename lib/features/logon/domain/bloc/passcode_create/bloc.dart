import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc//bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/passcode.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/l10n/localization.dart';

class PasscodeCreateBloc
    extends Bloc<PasscodeCreateEvent, PasscodeCreateState> {
  static const int MIN_PASSCODE_LENGTH = 4;
  static const String EMPTY = '';

  final RegistrationRepository registrationRepository;
  final LogonRepository logonRepository;
  final AppLocalization appLocalization;

  PasscodeCreateBloc({
    @required this.appLocalization,
    @required this.registrationRepository,
    @required this.logonRepository,
  })  : assert(registrationRepository != null),
        assert(AppLocalization != null),
        assert(logonRepository != null);

  @override
  PasscodeCreateState get initialState => PasscodeCreateState.init().copyWith(
        errorMassageValidatePasscode1:
            appLocalization.logonMessagePasscodeEmpty(),
        errorMassageValidatePasscode2:
            appLocalization.logonMessagePasscodeEmpty(),
      );

  @override
  Stream<PasscodeCreateState> mapEventToState(
      PasscodeCreateEvent event) async* {
    if (event is PasscodeCreateChanged1) {
      yield* _validatePasscode1(event.passcode1);
    }

    if (event is PasscodeCreateChanged2) {
      yield* _validatePasscode2(state.passcode1, event.passcode2);
    }

    if (event is PasscodeCreateSubmit) {
      yield* _createPasscode(logonRepository.registration, event.passcode1);
    }

    if (event is PasscodeCreateCancel) {
      yield* _createCancel(logonRepository.registration);
    }
  }

  Stream<PasscodeCreateState> _createPasscode(
      Registration registration, String passcode) async* {
    yield state.copyWith(
      createPasscodeStart: true,
      createPasscodeFailure: false,
      createPasscodeSuccess: false,
    );
    final result = await registrationRepository.storeRegistration(
      registration,
      Passcode(passcode),
    );
    yield result.fold(
      (failure) => state.copyWith(
        createPasscodeStart: false,
        createPasscodeFailure: true,
        errorMessageCreatePasscode: failure.message,
        createPasscodeSuccess: false,
      ),
      (success) => state.copyWith(
        createPasscodeStart: false,
        createPasscodeFailure: false,
        createPasscodeSuccess: true,
      ),
    );
    yield state.copyWith(
      createPasscodeStart: false,
      createPasscodeFailure: false,
      createPasscodeSuccess: false,
      errorMessageCreatePasscode: '',
    );
  }

  Stream<PasscodeCreateState> _createCancel(Registration registration) async* {
    yield state.copyWith(
      unregisterStart: true,
      unregisterFailure: false,
      unregisterSuccess: false,
    );
    final result = await registrationRepository.unregister(registration);
    yield result.fold(
      (failure) => state.copyWith(
          unregisterStart: false,
          unregisterFailure: true,
          unregisterSuccess: false,
          errorMessageUnregister: failure.message),
      (sucess) => state.copyWith(
        unregisterStart: false,
        unregisterFailure: false,
        unregisterSuccess: true,
      ),
    );
    yield state.copyWith(
      unregisterStart: false,
      unregisterFailure: false,
      unregisterSuccess: false,
      errorMessageUnregister: '',
    );
  }

  Stream<PasscodeCreateState> _validatePasscode1(String passcode1) async* {
    if (passcode1.isEmpty) {
      yield state.copyWith(
        passcode1: passcode1,
        isValidPasscode1: false,
        isAutoValidatePasscode1: true,
        errorMassageValidatePasscode1:
            appLocalization.logonMessagePasscodeEmpty(),
      );
    } else if (passcode1.length < MIN_PASSCODE_LENGTH) {
      yield state.copyWith(
        passcode1: passcode1,
        isAutoValidatePasscode1: true,
        isValidPasscode1: false,
        errorMassageValidatePasscode1:
            appLocalization.logonMessagePasscodeTooShorte(MIN_PASSCODE_LENGTH),
      );
    } else {
      yield state.copyWith(
        passcode1: passcode1,
        isAutoValidatePasscode1: true,
        isValidPasscode1: true,
        errorMassageValidatePasscode1: EMPTY,
      );
    }
  }

  Stream<PasscodeCreateState> _validatePasscode2(
      String passcode1, String passcode2) async* {
    if (passcode2.isEmpty) {
      yield state.copyWith(
        passcode2: passcode2,
        isAutoValidatePasscode2: true,
        isValidPasscode2: false,
        errorMassageValidatePasscode2:
            appLocalization.logonMessagePasscodeEmpty(),
      );
    } else if (passcode1 != passcode2) {
      yield state.copyWith(
        passcode2: passcode2,
        isValidPasscode2: false,
        isAutoValidatePasscode2: true,
        errorMassageValidatePasscode2:
            appLocalization.logonMessagePasscodeNotMatch(),
      );
    } else {
      yield state.copyWith(
        passcode2: passcode2,
        isValidPasscode2: true,
        isAutoValidatePasscode2: true,
        errorMassageValidatePasscode2: EMPTY,
      );
    }
  }
}
