import 'package:equatable/equatable.dart';

abstract class PasscodeCreateEvent extends Equatable {
  const PasscodeCreateEvent();

  @override
  List<Object> get props => [];
}

class PasscodeCreateSubmit extends PasscodeCreateEvent {
  final String passcode1;
  final String passcode2;

  const PasscodeCreateSubmit({this.passcode1, this.passcode2});

  @override
  List<Object> get props => [passcode1, passcode2];
}

class PasscodeCreateChanged1 extends PasscodeCreateEvent {
  final String passcode1;

  const PasscodeCreateChanged1({this.passcode1});

  @override
  List<Object> get props => [passcode1];
}

class PasscodeCreateChanged2 extends PasscodeCreateEvent {
  final String passcode2;

  const PasscodeCreateChanged2({this.passcode2});

  @override
  List<Object> get props => [passcode2];
}


class PasscodeCreateCancel extends PasscodeCreateEvent {}