import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class PasscodeCreateState extends Equatable {
  final String passcode1;
  final String passcode2;
  final bool isValidPasscode1;
  final bool isValidPasscode2;
  final String errorMassageValidatePasscode1;
  final String errorMassageValidatePasscode2;
  final bool createPasscodeStart;
  final bool createPasscodeSuccess;
  final bool createPasscodeFailure;
  final String errorMessageCreatePasscode;
  final bool unregisterStart;
  final bool unregisterSuccess;
  final bool unregisterFailure;
  final String errorMessageUnregister;
  final bool isAutoValidatePasscode1;
  final bool isAutoValidatePasscode2;

  bool get isValidForm => this.isValidPasscode1 && this.isValidPasscode2;

  PasscodeCreateState({
    @required this.passcode1,
    @required this.passcode2,
    @required this.isValidPasscode1,
    @required this.isValidPasscode2,
    @required this.errorMassageValidatePasscode1,
    @required this.errorMassageValidatePasscode2,
    @required this.createPasscodeStart,
    @required this.createPasscodeSuccess,
    @required this.createPasscodeFailure,
    @required this.errorMessageCreatePasscode,
    @required this.unregisterStart,
    @required this.unregisterSuccess,
    @required this.unregisterFailure,
    @required this.errorMessageUnregister,
    @required this.isAutoValidatePasscode1,
    @required this.isAutoValidatePasscode2,
  });

  factory PasscodeCreateState.init() {
    return PasscodeCreateState(
      errorMessageUnregister: '',
      unregisterStart: false,
      unregisterSuccess: false,
      unregisterFailure: false,
      createPasscodeFailure: false,
      createPasscodeStart: false,
      createPasscodeSuccess: false,
      errorMassageValidatePasscode1: '',
      errorMassageValidatePasscode2: '',
      errorMessageCreatePasscode: '',
      isValidPasscode1: false,
      isValidPasscode2: false,
      passcode1: '',
      passcode2: '',
      isAutoValidatePasscode1: false,
      isAutoValidatePasscode2: false,
    );
  }

  PasscodeCreateState copyWith({
    String passcode1,
    String passcode2,
    bool isValidPasscode1,
    bool isValidPasscode2,
    String errorMassageValidatePasscode1,
    String errorMassageValidatePasscode2,
    bool createPasscodeStart,
    bool createPasscodeSuccess,
    bool createPasscodeFailure,
    String errorMessageCreatePasscode,
    bool unregisterStart,
    bool unregisterSuccess,
    bool unregisterFailure,
    String errorMessageUnregister,
    bool isAutoValidatePasscode1,
    bool isAutoValidatePasscode2,
  }) {
    return PasscodeCreateState(
      passcode1: passcode1 ?? this.passcode1,
      passcode2: passcode2 ?? this.passcode2,
      isValidPasscode1: isValidPasscode1 ?? this.isValidPasscode1,
      isValidPasscode2: isValidPasscode2 ?? this.isValidPasscode2,
      errorMassageValidatePasscode1:
          errorMassageValidatePasscode1 ?? this.errorMassageValidatePasscode1,
      errorMassageValidatePasscode2:
          errorMassageValidatePasscode2 ?? this.errorMassageValidatePasscode2,
      createPasscodeStart: createPasscodeStart ?? this.createPasscodeStart,
      createPasscodeSuccess:
          createPasscodeSuccess ?? this.createPasscodeSuccess,
      createPasscodeFailure:
          createPasscodeFailure ?? this.createPasscodeFailure,
      errorMessageCreatePasscode:
          errorMessageCreatePasscode ?? this.errorMessageCreatePasscode,
      unregisterStart: unregisterStart ?? this.unregisterStart,
      unregisterFailure: unregisterFailure ?? this.unregisterFailure,
      unregisterSuccess: unregisterSuccess ?? this.unregisterSuccess,
      errorMessageUnregister:
          errorMessageUnregister ?? this.errorMessageUnregister,
      isAutoValidatePasscode1:
          isAutoValidatePasscode1 ?? this.isAutoValidatePasscode1,
      isAutoValidatePasscode2:
          isAutoValidatePasscode2 ?? this.isAutoValidatePasscode2,
    );
  }

  @override
  List<Object> get props => [
        passcode1,
        passcode2,
        isValidPasscode1,
        isValidPasscode2,
        errorMassageValidatePasscode1,
        errorMassageValidatePasscode2,
        createPasscodeFailure,
        createPasscodeStart,
        createPasscodeSuccess,
        errorMessageCreatePasscode,
        unregisterStart,
        unregisterFailure,
        unregisterSuccess,
        errorMessageUnregister,
        isAutoValidatePasscode1,
        isAutoValidatePasscode2,
      ];

  @override
  String toString() {
    return 'passcode1:$passcode1, passcode2:$passcode2, isValidPasscode1:$isValidPasscode1, isValidPasscode2:$isValidPasscode2, ' +
        'errorMassageValidatePasscode1:$errorMassageValidatePasscode1, errorMassageValidatePasscode2:$errorMassageValidatePasscode2, ' +
        'createPasscodeFailure:$createPasscodeFailure, createPasscodeStart:$createPasscodeStart, createPasscodeSuccess:$createPasscodeSuccess, ' +
        'errorMessageCreatePasscode:$errorMessageCreatePasscode, unregisterStart:$unregisterStart, unregisterFailure:$unregisterFailure, ' +
        'unregisterSuccess:$unregisterSuccess, errorMessageUnregister: $errorMessageUnregister' + 
        'isAutoValidatePasscode1: $isAutoValidatePasscode1, isAutoValidatePasscode2: $isAutoValidatePasscode2';
  }
}
