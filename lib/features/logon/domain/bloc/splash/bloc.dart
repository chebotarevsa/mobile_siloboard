import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/splash/event.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/splash/state.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final LogonRepository logonRepository;
  final RegistrationRepository registrationRepository;

  SplashBloc(
      {@required this.logonRepository, @required this.registrationRepository})
      : assert(logonRepository != null),
        assert(registrationRepository != null);

  @override
  SplashState get initialState => SplashState.UNDEFINED;

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (logonRepository.hasRegistration()) {
      yield SplashState.LOGINED;
    }

    if (event is SplashLogonStarted) {
      final result = await registrationRepository.hasRegistration();
      yield result.fold(
          (failur) => SplashState.FAILED,
          (storeStatus) =>
              storeStatus ? SplashState.REGISTRED : SplashState.UNREGISTRED);
    }
  }
}
