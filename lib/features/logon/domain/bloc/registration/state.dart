import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RegistrationState extends Equatable {
  final String username;
  final bool isValidUsername;
  final String errorMessageValidateUsername;
  final String password;
  final bool isValidPassword;
  final String errorMessageValidPassword;
  final bool registrationStart;
  final bool registrationSuccess;
  final bool registrationFailure;
  final String registrationErrorMessage;
  final bool isAutoValidateUsername;
  final bool isAutoValidatePassword;

  bool get isValidForm => isValidPassword && isValidUsername;

  RegistrationState(
      {@required this.username,
      @required this.isValidUsername,
      @required this.errorMessageValidateUsername,
      @required this.password,
      @required this.isValidPassword,
      @required this.errorMessageValidPassword,
      @required this.registrationStart,
      @required this.registrationSuccess,
      @required this.registrationFailure,
      @required this.registrationErrorMessage,
      @required this.isAutoValidateUsername,
      @required this.isAutoValidatePassword});

  factory RegistrationState.init() {
    return RegistrationState(
      username: '',
      password: '',
      isValidPassword: false,
      isValidUsername: false,
      errorMessageValidateUsername: '',
      errorMessageValidPassword: '',
      registrationFailure: false,
      registrationSuccess: false,
      registrationErrorMessage: '',
      registrationStart: false,
      isAutoValidateUsername: false,
      isAutoValidatePassword: false,
    );
  }

  RegistrationState copyWith({
    String username,
    bool isValidUsername,
    String errorMessageValidateUsername,
    String password,
    bool isValidPassword,
    String errorMessageValidPassword,
    bool registrationStart,
    bool registrationFailure,
    bool registrationSuccess,
    String registrationErrorMessage,
    bool isAutoValidateUsername,
    bool isAutoValidatePassword,
  }) {
    return RegistrationState(
      username: username ?? this.username,
      isValidUsername: isValidUsername ?? this.isValidUsername,
      errorMessageValidateUsername:
          errorMessageValidateUsername ?? this.errorMessageValidateUsername,
      password: password ?? this.password,
      isValidPassword: isValidPassword ?? this.isValidPassword,
      errorMessageValidPassword:
          errorMessageValidPassword ?? this.errorMessageValidPassword,
      registrationSuccess: registrationSuccess ?? this.registrationSuccess,
      registrationStart: registrationStart ?? this.registrationStart,
      registrationFailure: registrationFailure ?? this.registrationFailure,
      registrationErrorMessage:
          registrationErrorMessage ?? this.registrationErrorMessage,
      isAutoValidatePassword:
          isAutoValidatePassword ?? this.isAutoValidatePassword,
      isAutoValidateUsername:
          isAutoValidateUsername ?? this.isAutoValidateUsername,
    );
  }

  @override
  String toString() {
    return "username:$username, isValidUsername:$isValidUsername," +
        " errorMessageValidateUsername:$errorMessageValidateUsername, password:$password," +
        " isValidPassword:$isValidPassword, errorMessageValidPassword:$errorMessageValidPassword" +
        " registrationSuccess:$registrationSuccess, registrationStart: $registrationStart" +
        " registrationFailure:$registrationFailure, registrationErrorMessage:$registrationErrorMessage" + 
        " isAutoValidatePassword: $isAutoValidatePassword, isAutoValidateUsername: $isAutoValidateUsername";
  }

  @override
  List<Object> get props => [
        username,
        isValidUsername,
        password,
        isValidPassword,
        errorMessageValidateUsername,
        errorMessageValidPassword,
        registrationFailure,
        registrationSuccess,
        registrationErrorMessage,
        registrationStart,
        isAutoValidateUsername,
        isAutoValidatePassword,
      ];
}
