import 'package:equatable/equatable.dart';

abstract class RegistrationEvent extends Equatable {
  const RegistrationEvent();

  @override
  List<Object> get props => [];
}

class RegistrationSubmit extends RegistrationEvent {
  final String username;
  final String password;

  const RegistrationSubmit({this.username, this.password});

  @override
  List<Object> get props => [username, password];
}


class RegistrationUsernameChanged extends RegistrationEvent {
  final String username;

  const RegistrationUsernameChanged({this.username});

  @override
  List<Object> get props => [username];
}

class RegistrationPasswordChanged extends RegistrationEvent {
  final String password;

  const RegistrationPasswordChanged({this.password});

  @override
  List<Object> get props => [password];
}

