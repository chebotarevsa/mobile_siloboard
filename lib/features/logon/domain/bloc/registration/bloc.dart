import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/registration/event.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/registration/state.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/credential.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/l10n/localization.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  static const int MAX_USERNAME_LENGTH = 12;
  static const String MESSAGE_EMPTY = '';

  final LogonRepository logonRepository;
  final RegistrationRepository registrationRepository;
  final AppLocalization appLocalization;

  RegistrationBloc({
    @required this.logonRepository,
    @required this.registrationRepository,
    @required this.appLocalization,
  })  : assert(registrationRepository != null),
        assert(logonRepository != null),
        assert(appLocalization != null);

  @override
  RegistrationState get initialState => RegistrationState.init().copyWith(
        errorMessageValidateUsername: appLocalization.logonMessageUsernameEmpty(),
        errorMessageValidPassword: appLocalization.logonMessagePasswordEmpty(),
      );

  @override
  Stream<RegistrationState> mapEventToState(RegistrationEvent event) async* {
    if (event is RegistrationPasswordChanged) {
      yield* _validatePassword(event.password);
    }

    if (event is RegistrationUsernameChanged) {
      yield* _validateUserName(event.username);
    }

    if (event is RegistrationSubmit) {
      yield* _registrationSubmit(event.username, event.password);
    }
  }

  Stream<RegistrationState> _registrationSubmit(
      String username, String password) async* {
    yield state.copyWith(
      registrationStart: true,
      registrationFailure: false,
      registrationSuccess: false,
      registrationErrorMessage: '',
    );
    final result = await registrationRepository.register(Credential(
      password: password,
      username: username,
    ));
    yield result.fold(
        (failure) => state.copyWith(
              registrationFailure: true,
              registrationSuccess: false,
              registrationStart: false,
              registrationErrorMessage: failure.message,
            ), (registration) {
      logonRepository.registration = registration;
      return state.copyWith(
        registrationSuccess: true,
        registrationFailure: false,
        registrationStart: false,
      );
    });
    yield state.copyWith(
      registrationStart: false,
      registrationFailure: false,
      registrationSuccess: false,
      registrationErrorMessage: MESSAGE_EMPTY,
    );
  }

  Stream<RegistrationState> _validateUserName(String username) async* {
    RegExp pattern = new RegExp(r'^[a-zA-Z0-9]*$');
    if (username.isEmpty) {
      yield state.copyWith(
          username: username,
          isAutoValidateUsername: true,
          isValidUsername: false,
          errorMessageValidateUsername: appLocalization.logonMessageUsernameEmpty());
    } else if (username.length > MAX_USERNAME_LENGTH) {
      yield state.copyWith(
          username: username,
          isAutoValidateUsername: true,
          isValidUsername: false,
          errorMessageValidateUsername: appLocalization.logonMessageUsernameTooLong(MAX_USERNAME_LENGTH));
    } else if (!pattern.hasMatch(username)) {
      yield state.copyWith(
          username: username,
          isAutoValidateUsername: true,
          isValidUsername: false,
          errorMessageValidateUsername: appLocalization.logonMessageUsernameUnexpected());
    } else {
      yield state.copyWith(
          username: username,
          isAutoValidateUsername: true,
          isValidUsername: true,
          errorMessageValidateUsername: MESSAGE_EMPTY);
    }
  }

  Stream<RegistrationState> _validatePassword(String password) async* {
    if (password.isNotEmpty) {
      yield state.copyWith(
        password: password,
        isAutoValidatePassword: true,
        isValidPassword: true,
        errorMessageValidPassword: MESSAGE_EMPTY,
      );
    } else {
      yield state.copyWith(
        password: password,
        isAutoValidatePassword: true,
        isValidPassword: false,
        errorMessageValidPassword: appLocalization.logonMessagePasswordEmpty(),
      );
    }
  }
}
