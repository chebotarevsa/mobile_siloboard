import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';

abstract class LogonRepository {
  Registration get registration;
  set registration(Registration registration);
  bool hasRegistration();
  void clearRegistration();
}