import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/credential.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/passcode.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';

class Success extends Equatable {
  @override
  List<Object> get props => [];
}

class Failure extends Equatable {
  final String message;
  final Exception reason;

  Failure(this.message, {this.reason});

  @override
  String toString() {
    return this.message;
  }

  @override
  List<Object> get props => [message, reason];
}

abstract class RegistrationRepository {
  Future<Either<Failure, Registration>> register(Credential credential);
  Future<Either<Failure, Success>> unregister(Registration registration);
  Future<Either<Failure, bool>> hasRegistration();
  Future<Either<Failure, Success>> storeRegistration(
      Registration registration, Passcode passcode);
  Future<Either<Failure, Registration>> restoreRegistration(Passcode passcode);
  Future<Either<Failure, Success>> removeRegistration();
}
