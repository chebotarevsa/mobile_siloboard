import 'dart:convert';
import 'package:meta/meta.dart';
import 'package:flutter_string_encryption/flutter_string_encryption.dart';
import 'package:mobile_siloboard/features/logon/data/model/registration_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class RegistrationEncryptedPersistDataSource {
  Future<void> store(RegistrationModel credentialModel, String passcode);

  Future<RegistrationModel> restore(String passcode);

  Future<void> remove();

  Future<bool> hasStored();
}

class PersistException implements Exception {}

class CredentialEncryptedStoreSharedPreferencesDataSource
    implements RegistrationEncryptedPersistDataSource {
  static const String TAG_AUTH = 'AUTH';
  static const String TAG_SALT = 'SALT';

  final SharedPreferences _sharedPreferences;
  final PlatformStringCryptor _platformStringCryptor;
  final JsonCodec _json;

  CredentialEncryptedStoreSharedPreferencesDataSource(
      {@required SharedPreferences sharedPreferences,
      @required PlatformStringCryptor cryptor,
      @required JsonCodec json})
      : assert(cryptor != null),
        assert(sharedPreferences != null),
        assert(json != null),
        this._json = json,
        this._platformStringCryptor = cryptor,
        this._sharedPreferences = sharedPreferences;

  @override
  Future<RegistrationModel> restore(String passode) async {
    try {
      final String encryptedAuthStr = _sharedPreferences.getString(TAG_AUTH);
      final String salt = _sharedPreferences.getString(TAG_SALT);
      final String decryptKey =
          await _platformStringCryptor.generateKeyFromPassword(passode, salt);
      final String decryptedAuthStr =
          await _platformStringCryptor.decrypt(encryptedAuthStr, decryptKey);
      return RegistrationModel.fromJson(_json.decode(decryptedAuthStr));
    } on Exception {
      throw PersistException();
    }
  }

  @override
  Future<void> store(
      RegistrationModel registrationModel, String passcode) async {
    try {
      final String salt = await _platformStringCryptor.generateSalt();
      final String encryptKey =
          await _platformStringCryptor.generateKeyFromPassword(passcode, salt);
      final String authModelStr = _json.encode(registrationModel);
      final String encryptedAuthStr =
          await _platformStringCryptor.encrypt(authModelStr, encryptKey);

      if (!(await _sharedPreferences.setString(TAG_AUTH, encryptedAuthStr) &&
          await _sharedPreferences.setString(TAG_SALT, salt))) {
        throw PersistException();
      }
    } on Exception {
      throw PersistException();
    }
  }

  @override
  Future<bool> hasStored() async {
    return _sharedPreferences.containsKey(TAG_AUTH);
  }

  @override
  Future<void> remove() async {
    if (! await hasStored()){
      return;
    }
    if (!(await _sharedPreferences.remove(TAG_AUTH) &&
        await _sharedPreferences.remove(TAG_SALT))) {
      throw PersistException();
    }
  }
}
