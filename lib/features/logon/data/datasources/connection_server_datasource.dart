import 'package:flutter/cupertino.dart';
import 'package:mobile_siloboard/features/logon/data/model/connection_model.dart';
import 'package:mobile_siloboard/features/settings/domian/repository/application_settings_repository.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'dart:io' show Platform;

abstract class ConnectionServerDataSource {
  Future<ConnectionModel> createConnection(String login, String password);

  Future<ConnectionModel> readConnection(
      String login, String password, String appCId);

  Future<void> deleteConnection(String login, String password, String appCId);

  Future<void> updateConnection(String login, String password, String appCId,
      Map<String, dynamic> params);
}

class HttpException implements Exception {
  final String _host;
  final String _message;
  final String _reasons;

  HttpException({String host, String message, String reasons})
      : this._host = host,
        this._message = message,
        this._reasons = reasons;

  @override
  String toString() {
    return '$_host: $_message $_reasons';
  }
}

class ConnectionException implements Exception {
  final int _statusCode;
  final String _message;

  ConnectionException({int statusCode, String message})
      : this._statusCode = statusCode,
        this._message = message;

  @override
  String toString() {
    return '$_statusCode: $_message';
  }
}

class ConnectionApplicationNotFoundException implements Exception {
  final int _statusCode;

  ConnectionApplicationNotFoundException({int statusCode = 404})
      : this._statusCode = statusCode;

  @override
  String toString() {
    return '$_statusCode: Приложение не зарегистрировано на сервере';
  }
}

class ConnectionAuthenticationException implements Exception {
  final int _statusCode;

  ConnectionAuthenticationException({
    int statusCode = 401,
  }) : this._statusCode = statusCode;

  @override
  String toString() {
    return '$_statusCode: Не верные имя пользователя или пароль';
  }
}

class ConnectionSMPServerDataSource implements ConnectionServerDataSource {
  final ApplicationSettingsRepository _applicationSettingsRepository;
  final Client _client;

  ConnectionSMPServerDataSource({
    @required ApplicationSettingsRepository applicationSettingsRepository,
    @required Client client,
  })  : assert(applicationSettingsRepository != null),
        assert(client != null),
        _applicationSettingsRepository = applicationSettingsRepository,
        _client = client;

  _throwConnectException(int statusCode, String body) {
    switch (statusCode) {
      case 401:
        throw ConnectionAuthenticationException();
      case 404:
        throw ConnectionApplicationNotFoundException();
      default:
        ConnectionException(statusCode: statusCode, message: body);
    }
  }

  _throwHttpException(String reasons) async {
    throw HttpException(
        host: (await _applicationSettingsRepository.getApplicationSettings())
            .host,
        message: 'Недоступен',
        reasons: reasons);
  }

  Future<Uri> _buildUrl([String appCId]) async {
    var applicationSettings =
        await _applicationSettingsRepository.getApplicationSettings();
    String appCIdStr = appCId != null ? '(\'$appCId\')' : '';
    return Uri(
        scheme: applicationSettings.https ? 'https' : 'http',
        host: applicationSettings.host,
        port: applicationSettings.port,
        path:
            'odata/applications/v4/${applicationSettings.appId}/Connections$appCIdStr');
  }

  String _getAuthString(username, password) {
    final token = base64.encode(utf8.encode('$username:$password'));
    final authStr = 'Basic ' + token.trim();
    return authStr;
  }

  Map<String, String> _buildHeaders(String login, String password,
      [String appCId]) {
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': _getAuthString(login, password)
    };
    if (appCId != null) {
      headers['X-SMP-APPCID'] = appCId;
    }
    return headers;
  }

  @override
  Future<ConnectionModel> createConnection(
      String login, String password) async {
    String body = jsonEncode({'DeviceType': Platform.operatingSystem});
    Response response;
    try {
      response = await _client.post(await _buildUrl(),
          headers: _buildHeaders(login, password), body: body);
    } catch (e) {
      await _throwHttpException(e.toString());
    }

    if (response.statusCode != 201) {
      _throwConnectException(response.statusCode, response.body);
    }
    Map<String, dynamic> resultJson = jsonDecode(response.body);
    return ConnectionModel.fromJson(resultJson['d']);
  }

  @override
  Future<void> deleteConnection(
      String login, String password, String appCId) async {
    Response response;
    try {
      response = await _client.delete(await _buildUrl(appCId),
          headers: _buildHeaders(login, password, appCId));
    } catch (e) {
      await _throwHttpException(e.toString());
    }

    if (response.statusCode != 200) {
      _throwConnectException(response.statusCode, response.body);
    }
  }

  @override
  Future<ConnectionModel> readConnection(
      String login, String password, String appCId) async {
    Response response;
    try {
      response = await _client.get(await _buildUrl(appCId),
          headers: _buildHeaders(login, password, appCId));
    } catch (e) {
      await _throwHttpException(e.toString());
    }
    if (response.statusCode != 200) {
      _throwConnectException(response.statusCode, response.body);
    }
    Map<String, dynamic> resultJson = jsonDecode(response.body);
    return ConnectionModel.fromJson(resultJson['d']);
  }

  @override
  Future<void> updateConnection(String login, String password, String appCId,
      Map<String, dynamic> params) async {
    String body = jsonEncode(params);
    Response response;
    try {
      response = await _client.put(await _buildUrl(appCId),
          headers: _buildHeaders(login, password, appCId), body: body);
    } catch (e) {
      await _throwHttpException(e.toString());
    }
    if (response.statusCode != 200) {
      _throwConnectException(response.statusCode, response.body);
    }
  }
}
