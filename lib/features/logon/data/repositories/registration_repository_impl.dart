import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';

class LogonRepositoryImpl implements LogonRepository {
  Registration _registration;

  @override
  Registration get registration => this._registration;
  @override
  set registration(Registration registration) {
    this._registration = registration;
  }

  @override
  bool hasRegistration() {
    return this._registration != null;
  }

  @override
  void clearRegistration() {
    this._registration = null;
  }
}
