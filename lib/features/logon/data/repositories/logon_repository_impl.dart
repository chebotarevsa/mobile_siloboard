import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:mobile_siloboard/features/logon/data/datasources/connection_server_datasource.dart';
import 'package:mobile_siloboard/features/logon/data/datasources/registration_persist_datasource.dart';
import 'package:mobile_siloboard/features/logon/data/model/connection_model.dart';
import 'package:mobile_siloboard/features/logon/data/model/registration_model.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/credential.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/passcode.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';

class RegistrationRepositoryImpl implements RegistrationRepository {
  final ConnectionServerDataSource _connectionServerDataSource;
  final RegistrationEncryptedPersistDataSource
      _registrationEncryptedPersistDataSource;

  RegistrationRepositoryImpl(
      {@required connectionServerDataSource,
      @required registrationEncryptedPersistDataSource})
      : assert(registrationEncryptedPersistDataSource != null),
        assert(connectionServerDataSource != null),
        this._registrationEncryptedPersistDataSource =
            registrationEncryptedPersistDataSource,
        this._connectionServerDataSource = connectionServerDataSource;

  @override
  Future<Either<Failure, bool>> hasRegistration() async {
    try {
      final bool result =
          await this._registrationEncryptedPersistDataSource.hasStored();
      return Right(result);
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }

  @override
  Future<Either<Failure, Registration>> register(Credential credential) async {
    try {
      final ConnectionModel connectionModel = await _connectionServerDataSource
          .createConnection(credential.username, credential.password);
      return Right(Registration(
          username: credential.username,
          password: credential.password,
          appCId: connectionModel.applicationConnectionId));
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }

  @override
  Future<Either<Failure, Success>> removeRegistration() async {
    try {
      this._registrationEncryptedPersistDataSource.remove();
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
    return Right(Success());
  }

  @override
  Future<Either<Failure, Registration>> restoreRegistration(
      Passcode passcode) async {
    try {
      RegistrationModel registrationModel = await this
          ._registrationEncryptedPersistDataSource
          .restore(passcode.passcode);
      return Right(Registration(
        username: registrationModel.username,
        password: registrationModel.password,
        appCId: registrationModel.appCId,
      ));
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }

  @override
  Future<Either<Failure, Success>> storeRegistration(
      Registration registration, passcode) async {
    try {
      if (registration == null) throw Exception('Нет данных o регистрации');
      this._registrationEncryptedPersistDataSource.store(
          RegistrationModel(
            username: registration.username,
            password: registration.password,
            appCId: registration.appCId,
          ),
          passcode.passcode);
      return Right(Success());
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }

  @override
  Future<Either<Failure, Success>> unregister(Registration registration) async {
    if (registration == null)
      return Left(Failure(
        'Нет данных о регистрации',
      ));
    try {
      await _connectionServerDataSource.deleteConnection(
          registration.username, registration.password, registration.appCId);
      this.removeRegistration();
      return Right(Success());
    } catch (e) {
      return Left(Failure(e.toString(), reason: e));
    }
  }
}
