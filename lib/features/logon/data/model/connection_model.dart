import 'package:equatable/equatable.dart';

class ConnectionModel extends Equatable {
  final String applicationConnectionId;
  final String eTag;
  final String applicationVersion;
  final String userName;
  final String deviceModel;
  final String deviceType;
  final String deviceSubType;
  final String devicePhoneNumber;
  final String deviceIMSI;
  final bool passwordPolicyEnabled;
  final bool passwordPolicyDefaultPasswordAllowed;
  final bool passwordPolicyMinLength;
  final bool passwordPolicyDigitRequired;
  final bool passwordPolicyUpperRequired;
  final bool passwordPolicyLowerRequired;
  final bool passwordPolicySpecialRequired;
  final int passwordPolicyExpiresInNDays;
  final int passwordPolicyMinUniqueChars;
  final int passwordPolicyLockTimeout;
  final int passwordPolicyRetryLimit;
  final String proxyApplicationEndpoint;
  final String proxyPushEndpoint;
  final String uploadLogs;

  ConnectionModel({
    this.applicationConnectionId,
    this.eTag,
    this.applicationVersion,
    this.userName,
    this.deviceModel,
    this.deviceType,
    this.deviceSubType,
    this.devicePhoneNumber,
    this.deviceIMSI,
    this.passwordPolicyEnabled,
    this.passwordPolicyDefaultPasswordAllowed,
    this.passwordPolicyMinLength,
    this.passwordPolicyDigitRequired,
    this.passwordPolicyUpperRequired,
    this.passwordPolicyLowerRequired,
    this.passwordPolicySpecialRequired,
    this.passwordPolicyExpiresInNDays,
    this.passwordPolicyMinUniqueChars,
    this.passwordPolicyLockTimeout,
    this.passwordPolicyRetryLimit,
    this.proxyApplicationEndpoint,
    this.proxyPushEndpoint,
    this.uploadLogs,
  });

  factory ConnectionModel.fromJson(Map<String, dynamic> jsonMap) {
    return ConnectionModel(
      eTag: jsonMap["ETag"],
      applicationConnectionId: jsonMap["ApplicationConnectionId"],
      userName: jsonMap["UserName"],
      applicationVersion: jsonMap["ApplicationVersion"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "ApplicationConnectionId": this.applicationConnectionId,
      "ETag": this.eTag,
      "UserName": this.userName,
      "ApplicationVersion": this.applicationVersion
    };
  }

  @override
  List<Object> get props => [applicationConnectionId];
}
