import 'package:equatable/equatable.dart';

class RegistrationModel extends Equatable {
  final String username;
  final String password;
  final String appCId;

  RegistrationModel({this.username, this.password, this.appCId});

  factory RegistrationModel.fromJson(Map<String, dynamic> jsonMap) {
    return RegistrationModel(
      username: jsonMap["username"],
      password: jsonMap["password"],
      appCId: jsonMap["appCId"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "username": this.username,
      "password": this.password,
      "appCId": this.appCId,
    };
  }

  @override
  List<Object> get props => [username, password, appCId];
}
