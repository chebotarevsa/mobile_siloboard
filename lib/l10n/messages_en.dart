// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(minLength) => "Passcode less than ${minLength}";

  static m1(maxLength) => "Length greater than ${maxLength}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "logonHintEnterPasscode" : MessageLookupByLibrary.simpleMessage("Enter Passcode"),
    "logonHintRepeatPasscode" : MessageLookupByLibrary.simpleMessage("Repeat Passcode"),
    "logonHintSetPasscode" : MessageLookupByLibrary.simpleMessage("Set Passcode"),
    "logonLabelPasscode" : MessageLookupByLibrary.simpleMessage("Passcode"),
    "logonLabelPasscodeRepeat" : MessageLookupByLibrary.simpleMessage("Passcode repeat"),
    "logonLabelPassword" : MessageLookupByLibrary.simpleMessage("Password"),
    "logonLabelUsername" : MessageLookupByLibrary.simpleMessage("Username"),
    "logonMessageInvalidPasscode" : MessageLookupByLibrary.simpleMessage("Invalid Passcode"),
    "logonMessagePasscodeEmpty" : MessageLookupByLibrary.simpleMessage("Passcode is empty"),
    "logonMessagePasscodeNotMatch" : MessageLookupByLibrary.simpleMessage("Passcode don\t match"),
    "logonMessagePasscodeTooShorte" : m0,
    "logonMessagePasswordEmpty" : MessageLookupByLibrary.simpleMessage("Password is empty"),
    "logonMessageUsernameEmpty" : MessageLookupByLibrary.simpleMessage("Username is empty"),
    "logonMessageUsernameTooLong" : m1,
    "logonMessageUsernameUnexpected" : MessageLookupByLibrary.simpleMessage("Contained unexpected symbols"),
    "logonTextCancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "logonTextCancelOfRegistration" : MessageLookupByLibrary.simpleMessage("Are you cancel registration really?"),
    "logonTextContinue" : MessageLookupByLibrary.simpleMessage("Continue"),
    "logonTextEnter" : MessageLookupByLibrary.simpleMessage("Enter"),
    "logonTextForgotPasscode" : MessageLookupByLibrary.simpleMessage("Forgot?"),
    "logonTextNo" : MessageLookupByLibrary.simpleMessage("No"),
    "logonTextRergistration" : MessageLookupByLibrary.simpleMessage("Registration"),
    "logonTextResetPasscode" : MessageLookupByLibrary.simpleMessage("To reset the passcode, re-registration is required. All data on the device will be deleted"),
    "logonTextSet" : MessageLookupByLibrary.simpleMessage("Set"),
    "logonTextSettings" : MessageLookupByLibrary.simpleMessage("Settings"),
    "logonTextYes" : MessageLookupByLibrary.simpleMessage("Yes"),
    "logonTitleCancelOfRegistration" : MessageLookupByLibrary.simpleMessage("Cancel Of Registartion"),
    "logonTitleResetPasscode" : MessageLookupByLibrary.simpleMessage("Reset Passcode"),
    "settingsLabelApplicationId" : MessageLookupByLibrary.simpleMessage("Application Id"),
    "settingsLabelHTTPS" : MessageLookupByLibrary.simpleMessage("HTTPS"),
    "settingsLabelHost" : MessageLookupByLibrary.simpleMessage("Host"),
    "settingsLabelPort" : MessageLookupByLibrary.simpleMessage("Port"),
    "settingsMessageApplicationIdIsEmpty" : MessageLookupByLibrary.simpleMessage("Application Id is empty"),
    "settingsMessageHostIdIsEmpty" : MessageLookupByLibrary.simpleMessage("Host is empty"),
    "settingsMessageLoaded" : MessageLookupByLibrary.simpleMessage("Settings are loaded"),
    "settingsMessagePortIncorrent" : MessageLookupByLibrary.simpleMessage("Port incorrect"),
    "settingsMessageSaved" : MessageLookupByLibrary.simpleMessage("Settings are saved"),
    "settingsMessageSettingsDoNotLoaded" : MessageLookupByLibrary.simpleMessage("Setting no not loaded"),
    "settingsMessageSettingsDoNotSaved" : MessageLookupByLibrary.simpleMessage("Setting no not saved"),
    "settingsTextSettings" : MessageLookupByLibrary.simpleMessage("Settings"),
    "title" : MessageLookupByLibrary.simpleMessage("Siloboard")
  };
}
