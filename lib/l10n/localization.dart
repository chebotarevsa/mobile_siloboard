import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobile_siloboard/l10n/messages_all.dart';

class AppLocalization {
  final Locale locale;

  AppLocalization(this.locale);

  static const _AppLocalizationDelegate delegate =
      const _AppLocalizationDelegate();

  static Future<AppLocalization> load(Locale locale) async {
    final String name = locale.countryCode?.isEmpty ?? false
        ? locale.languageCode
        : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return AppLocalization(locale);
    });
  }

  static AppLocalization of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }

  title() => Intl.message(
        'Siloboard',
        name: 'title',
        desc: 'Title of application',
      );

  logonTitleCancelOfRegistration() => Intl.message(
        'Cancel Of Registartion',
        name: 'logonTitleCancelOfRegistration',
        desc: 'Dialog title Cancel Of Registration',
      );
  logonTitleResetPasscode() => Intl.message(
        'Reset Passcode',
        name: 'logonTitleResetPasscode',
        desc: 'Dialog title Reset Passcode',
      );

  logonTextResetPasscode() => Intl.message(
        'To reset the passcode, re-registration is required. All data on the device will be deleted',
        name: 'logonTextResetPasscode',
        desc: 'Dialog text Reset Passcode',
      );

  logonTextCancelOfRegistration() => Intl.message(
        'Are you cancel registration really?',
        name: 'logonTextCancelOfRegistration',
        desc: 'Dialog text Cancel Of Registration)',
      );

  logonTextYes() => Intl.message(
        'Yes',
        name: 'logonTextYes',
        desc: 'Yes',
      );

  logonTextNo() => Intl.message(
        'No',
        name: 'logonTextNo',
        desc: 'No',
      );

  logonTextContinue() => Intl.message(
        'Continue',
        name: 'logonTextContinue',
        desc: 'Continue',
      );

  logonTextEnter() => Intl.message(
        'Enter',
        name: 'logonTextEnter',
        desc: 'Enter',
      );

  logonTextForgotPasscode() => Intl.message(
        'Forgot?',
        name: 'logonTextForgotPasscode',
        desc: 'Forgot question',
      );

  logonMessageInvalidPasscode() => Intl.message(
        'Invalid Passcode',
        name: 'logonMessageInvalidPasscode',
        desc: 'Error message on validate passcode screen',
      );

  logonMessageUsernameEmpty() => Intl.message(
        'Username is empty',
        name: 'logonMessageUsernameEmpty',
        desc: 'Error message on registration screen',
      );

  logonMessageUsernameTooLong(int maxLength) => Intl.message(
        'Length greater than $maxLength',
        name: 'logonMessageUsernameTooLong',
        args: [maxLength],
        desc: 'Error message on registration screen',
      );

  logonMessageUsernameUnexpected() => Intl.message(
        'Contained unexpected symbols',
        name: 'logonMessageUsernameUnexpected',
        desc: 'Error message on registration screen',
      );

  logonMessagePasswordEmpty() => Intl.message(
        'Password is empty',
        name: 'logonMessagePasswordEmpty',
        desc: 'Error message on registration screen',
      );

  logonMessagePasscodeEmpty() => Intl.message(
        'Passcode is empty',
        name: 'logonMessagePasscodeEmpty',
        desc: 'Error message when passcode  is empty',
      );

  logonMessagePasscodeTooShorte(int minLength) => Intl.message(
        'Passcode less than $minLength',
        name: 'logonMessagePasscodeTooShorte',
        args: [minLength],
        desc: 'Error message when passcode is to shorte',
      );

  logonMessagePasscodeNotMatch() => Intl.message(
        'Passcode don\t match',
        name: 'logonMessagePasscodeNotMatch',
        desc: 'Error message when passcode don\'t match',
      );

  logonLabelUsername() => Intl.message(
        'Username',
        name: 'logonLabelUsername',
        desc: 'Username label',
      );
  logonLabelPassword() => Intl.message(
        'Password',
        name: 'logonLabelPassword',
        desc: 'Password label',
      );

  logonLabelPasscode() => Intl.message(
        'Passcode',
        name: 'logonLabelPasscode',
        desc: 'Passcode label',
      );

  logonLabelPasscodeRepeat() => Intl.message(
        'Passcode repeat',
        name: 'logonLabelPasscodeRepeat',
        desc: 'Passcode repeat label',
      );

  logonTextRergistration() => Intl.message(
        'Registration',
        name: 'logonTextRergistration',
        desc: 'Registration text',
      );

  logonTextSettings() => Intl.message(
        'Settings',
        name: 'logonTextSettings',
        desc: 'Settings text',
      );
  logonTextCancel() => Intl.message(
        'Cancel',
        name: 'logonTextCancel',
        desc: 'Cancel text',
      );

  logonTextSet() => Intl.message(
        'Set',
        name: 'logonTextSet',
        desc: 'Set text',
      );

  logonHintSetPasscode() => Intl.message(
        'Set Passcode',
        name: 'logonHintSetPasscode',
        desc: 'Set Passcode hint',
      );
  logonHintEnterPasscode() => Intl.message(
        'Enter Passcode',
        name: 'logonHintEnterPasscode',
        desc: 'Enter Passcode hint',
      );
  logonHintRepeatPasscode() => Intl.message(
        'Repeat Passcode',
        name: 'logonHintRepeatPasscode',
        desc: 'Repeat Passcode hint',
      );
  settingsMessageLoaded() => Intl.message(
        'Settings are loaded',
        name: 'settingsMessageLoaded',
        desc: 'Message when setting are loaded',
      );

  settingsMessageSaved() => Intl.message(
        'Settings are saved',
        name: 'settingsMessageSaved',
        desc: 'Message when setting are loaded',
      );
  settingsMessageApplicationIdIsEmpty() => Intl.message(
        'Application Id is empty',
        name: 'settingsMessageApplicationIdIsEmpty',
        desc: 'Message when Application Id is empty',
      );
  settingsMessageHostIdIsEmpty() => Intl.message(
        'Host is empty',
        name: 'settingsMessageHostIdIsEmpty',
        desc: 'Message when Host is empty',
      );

  settingsMessagePortIncorrent() => Intl.message(
        'Port incorrect',
        name: 'settingsMessagePortIncorrent',
        desc: 'Message when Port is empty',
      );

  settingsMessageSettingsDoNotSaved() => Intl.message(
        'Setting no not saved',
        name: 'settingsMessageSettingsDoNotSaved',
        desc: 'Message when setting do not saved',
      );
  settingsMessageSettingsDoNotLoaded() => Intl.message(
        'Setting no not loaded',
        name: 'settingsMessageSettingsDoNotLoaded',
        desc: 'Message when setting do not loaded',
      );

  settingsTextSettings() => Intl.message(
        'Settings',
        name: 'settingsTextSettings',
        desc: 'Text Settins',
      );

  settingsLabelApplicationId() => Intl.message(
        'Application Id',
        name: 'settingsLabelApplicationId',
        desc: 'Label Application Id',
      );
  settingsLabelHost() => Intl.message(
        'Host',
        name: 'settingsLabelHost',
        desc: 'Label Host',
      );
  settingsLabelPort() => Intl.message(
        'Port',
        name: 'settingsLabelPort',
        desc: 'Label Port',
      );
  settingsLabelHTTPS() => Intl.message(
        'HTTPS',
        name: 'settingsLabelHTTPS',
        desc: 'Label HTTPS',
      );
}

class _AppLocalizationDelegate extends LocalizationsDelegate<AppLocalization> {
  const _AppLocalizationDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<AppLocalization> load(Locale locale) => AppLocalization.load(locale);

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalization> old) => false;
}
