// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  static m0(minLength) => "ПИН код меньше ${minLength}";

  static m1(maxLength) => "Длина больше ${maxLength} символов";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "logonHintEnterPasscode" : MessageLookupByLibrary.simpleMessage("Введите ПИН-код"),
    "logonHintRepeatPasscode" : MessageLookupByLibrary.simpleMessage("Повторите ПИН-код"),
    "logonHintSetPasscode" : MessageLookupByLibrary.simpleMessage("Установить ПИН-код"),
    "logonLabelPasscode" : MessageLookupByLibrary.simpleMessage("ПИН-код"),
    "logonLabelPasscodeRepeat" : MessageLookupByLibrary.simpleMessage("ПИН-код повтор"),
    "logonLabelPassword" : MessageLookupByLibrary.simpleMessage("Пароль"),
    "logonLabelUsername" : MessageLookupByLibrary.simpleMessage("Имя пользователя"),
    "logonMessageInvalidPasscode" : MessageLookupByLibrary.simpleMessage("Не верный ПИН-код"),
    "logonMessagePasscodeEmpty" : MessageLookupByLibrary.simpleMessage("ПИН-Код не заполнен"),
    "logonMessagePasscodeNotMatch" : MessageLookupByLibrary.simpleMessage("ПИН-коды не совпадают"),
    "logonMessagePasscodeTooShorte" : m0,
    "logonMessagePasswordEmpty" : MessageLookupByLibrary.simpleMessage("Пароль не заполнен"),
    "logonMessageUsernameEmpty" : MessageLookupByLibrary.simpleMessage("Имя пользователя не заполнено"),
    "logonMessageUsernameTooLong" : m1,
    "logonMessageUsernameUnexpected" : MessageLookupByLibrary.simpleMessage("Содержит не допустимый символ"),
    "logonTextCancel" : MessageLookupByLibrary.simpleMessage("Отмена"),
    "logonTextCancelOfRegistration" : MessageLookupByLibrary.simpleMessage("Вы действительно хотите отменить регистацию?"),
    "logonTextContinue" : MessageLookupByLibrary.simpleMessage("Продолжить"),
    "logonTextEnter" : MessageLookupByLibrary.simpleMessage("Ввод"),
    "logonTextForgotPasscode" : MessageLookupByLibrary.simpleMessage("Забыли?"),
    "logonTextNo" : MessageLookupByLibrary.simpleMessage("Нет"),
    "logonTextRergistration" : MessageLookupByLibrary.simpleMessage("Рагистрация"),
    "logonTextResetPasscode" : MessageLookupByLibrary.simpleMessage("Для сброса ПИН-кода необходима повторная регистрация. Все данные на устройстве будут удалены"),
    "logonTextSet" : MessageLookupByLibrary.simpleMessage("Установить"),
    "logonTextSettings" : MessageLookupByLibrary.simpleMessage("Параметры"),
    "logonTextYes" : MessageLookupByLibrary.simpleMessage("Да"),
    "logonTitleCancelOfRegistration" : MessageLookupByLibrary.simpleMessage("Отмена регистации"),
    "logonTitleResetPasscode" : MessageLookupByLibrary.simpleMessage("Сброс ПИН-кода"),
    "settingsLabelApplicationId" : MessageLookupByLibrary.simpleMessage("Идентификатор приложения"),
    "settingsLabelHTTPS" : MessageLookupByLibrary.simpleMessage("HTTPS"),
    "settingsLabelHost" : MessageLookupByLibrary.simpleMessage("Хост"),
    "settingsLabelPort" : MessageLookupByLibrary.simpleMessage("Порт"),
    "settingsMessageApplicationIdIsEmpty" : MessageLookupByLibrary.simpleMessage("Идентификатор приложения не заполнен"),
    "settingsMessageHostIdIsEmpty" : MessageLookupByLibrary.simpleMessage("Имя хоста не заполнено"),
    "settingsMessageLoaded" : MessageLookupByLibrary.simpleMessage("Параметры загруженны"),
    "settingsMessagePortIncorrent" : MessageLookupByLibrary.simpleMessage("Порт не корректен"),
    "settingsMessageSaved" : MessageLookupByLibrary.simpleMessage("Параметры сохранены"),
    "settingsMessageSettingsDoNotLoaded" : MessageLookupByLibrary.simpleMessage("Параметры не загруженны"),
    "settingsMessageSettingsDoNotSaved" : MessageLookupByLibrary.simpleMessage("Параметры не сохранены"),
    "settingsTextSettings" : MessageLookupByLibrary.simpleMessage("Параметры"),
    "title" : MessageLookupByLibrary.simpleMessage("Силосная доска")
  };
}
