import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_siloboard/common/utils/odata_client/lib/service/filter.dart';


main() {
  test('Filter value ', () {
    print(10.5.runtimeType.toString());
    expect(FilterValue<String>.value('10').toString(), '\'10\'');
    expect(FilterValue<int>.value(10).toString(), '10');
  });

  test('Filter Entity ', () {
    expect(FilterProperty.name('Address/City').toString(), 'Address/City');
  });

  test('logic operation eq', () {
    expect(
        FilterLogic.eq(FilterProperty.name('Address/City'),
                FilterValue.value('Redmond'))
            .toString(),
        'Address/City eq \'Redmond\'');
  });
  test('logic operation ne', () {
    expect(
        FilterLogic.ne(FilterProperty.name('Address/City'),
                FilterValue.value('London'))
            .toString(),
        'Address/City ne \'London\'');
  });

  test('logic operation gt', () {
    expect(
        FilterLogic.gt(FilterProperty.name('Price'), FilterValue<int>.value(20))
            .toString(),
        'Price gt 20');
  });

  test('Group', () {
    expect(
        FilterLogic.gt(FilterProperty.name('Price'), FilterValue<int>.value(20))
            .toString(),
        'Price gt 20');
  });

  test('Group', () {
    print(FilterLogic.andAll([
      FilterLogic.gt(FilterProperty.name('Price'), FilterValue<int>.value(20)),
      FilterLogic.gt(FilterProperty.name('Price'),FilterValue<int>.value(40)),
      FilterLogic.gt(FilterProperty.name('Price'),FilterValue<int>.value(90)),
      FilterLogic.gt(FilterProperty.name('Price'),FilterValue<int>.value(60)),
    ]));
  });
}
