import 'dart:io';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mobile_siloboard/features/logon/data/datasources/connection_server_datasource.dart';
import 'package:mobile_siloboard/features/logon/data/model/connection_model.dart';
import 'package:mobile_siloboard/features/settings/domian/entity/application_settings.dart';
import 'package:mobile_siloboard/features/settings/domian/repository/application_settings_repository.dart';
import 'package:mockito/mockito.dart';
import '../../fixtures/fixture_reader.dart';

class MockApplicationSettingsRepository extends Mock
    implements ApplicationSettingsRepository {}

class MockClient extends Mock implements Client {}

main() {
  ConnectionServerDataSource connectionServerDataSource;
  ApplicationSettingsRepository applicationSettingsRepository;
  Client client;

  final ConnectionModel connectionModel = ConnectionModel(
    applicationConnectionId: 'de4ece90-82a8-4ecf-b90c-10a9b18179da',
    eTag: '2911219',
    userName: 'SCHEBOTAREV',
    applicationVersion: '1',
  );

  final ApplicationSettings applicationSettings = ApplicationSettings(
    appId: 'appId',
    host: 'test.com',
    https: true,
    port: 443,
  );

  final String username = 'username';
  final String password = 'password';
  final String appCId = 'de4ece90-82a8-4ecf-b90c-10a9b18179da';

  final Uri uri = Uri.parse(
      'https://test.com:443/odata/applications/v4/appId/Connections(\'de4ece90-82a8-4ecf-b90c-10a9b18179da\')');
  final Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
    'X-SMP-APPCID': 'de4ece90-82a8-4ecf-b90c-10a9b18179da'
  };

  final String body = '{"DeviceType":"Android"}';

  setUp(() {
    applicationSettingsRepository = MockApplicationSettingsRepository();
    client = MockClient();
    connectionServerDataSource = ConnectionSMPServerDataSource(
        applicationSettingsRepository: applicationSettingsRepository,
        client: client);
    when(applicationSettingsRepository.getApplicationSettings())
        .thenAnswer((_) async => applicationSettings);
  });
  //
  group('Create connection', () {
    final Uri createUri = Uri.parse(
        'https://test.com:443/odata/applications/v4/appId/Connections');
    final Map<String, String> createHeaders = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ=',
    };
    final String createBody = '{"DeviceType":"${Platform.operatingSystem}"}';

    test('Should call successful create connection', () async {
      when(client.post(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenAnswer((_) async => Response(fixture('connection.json'), 201));

      ConnectionModel result =
          await connectionServerDataSource.createConnection(username, password);
      expect(result, connectionModel);
      verify(client.post(createUri, headers: createHeaders, body: createBody));
      verify(applicationSettingsRepository.getApplicationSettings());
    });

    test('Should throw when http connection failed', () async {
      when(client.post(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenThrow(Exception());

      await expectLater(
          () async => await 
              connectionServerDataSource.createConnection(username, password),
          throwsA(isInstanceOf<HttpException>()));
      verify(
          client.post(createUri, headers: createHeaders, body: createBody));
    });

    test('Should throw when create failed 401', () async {
      when(client.post(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenAnswer((_) async => Response('', 401));

      await expectLater(
          () async => await
              connectionServerDataSource.createConnection(username, password),
          throwsA(isInstanceOf<ConnectionAuthenticationException>()));
      verify(
          client.post(createUri, headers: createHeaders, body: createBody));
    });

    test('Should throw when create failed 404', () async {
      when(client.post(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenAnswer((_) async => Response('', 404));

      await expectLater(
          () async =>
              await connectionServerDataSource.createConnection(username, password),
          throwsA(isInstanceOf<ConnectionApplicationNotFoundException>()));
      verify(
          client.post(createUri, headers: createHeaders, body: createBody));
    });
  });
  //
  group('Delete connection', () {
    test('Should call successful delete connection', () async {
      when(client.delete(
        any,
        headers: anyNamed('headers'),
      )).thenAnswer((_) async => Response('', 201));

      await connectionServerDataSource.deleteConnection(
          username, password, appCId);

      verify(client.delete(uri, headers: headers));
      verify(applicationSettingsRepository.getApplicationSettings());
    });

    test('Should throw when http connection failed', () async {
      when(client.delete(any, headers: anyNamed('headers')))
          .thenThrow(Exception());

      await expectLater(
          () async => await connectionServerDataSource.deleteConnection(
              username, password, appCId),
          throwsA(isInstanceOf<HttpException>()));
      verify(client.delete(uri, headers: headers));
    });

    test('Should throw when delete failed 401', () async {
      when(client.delete(any, headers: anyNamed('headers')))
          .thenAnswer((_) async => Response('', 401));

      await expectLater(
          () async => await connectionServerDataSource.deleteConnection(
              username, password, appCId),
          throwsA(isInstanceOf<ConnectionAuthenticationException>()));
      verify(client.delete(uri, headers: headers));
    });

    test('Should throw when delete failed 404', () async {
      when(client.delete(any, headers: anyNamed('headers')))
          .thenAnswer((_) async => Response('', 404));

      await expectLater(
          () async => await connectionServerDataSource.deleteConnection(
              username, password, appCId),
          throwsA(isInstanceOf<ConnectionApplicationNotFoundException>()));
      verify(client.delete(uri, headers: headers));
    });
  });
  //
  group('Read Connection', () {
    test('Should call successful read connection', () async {
      when(client.get(
        any,
        headers: anyNamed('headers'),
      )).thenAnswer((_) async => Response(fixture('connection.json'), 200));

      ConnectionModel result = await connectionServerDataSource.readConnection(
          username, password, appCId);
      expect(result, connectionModel);
      expect(result.applicationConnectionId, appCId);

      verify(client.get(uri, headers: headers));
      verify(applicationSettingsRepository.getApplicationSettings());
    });

    test('Should throw when http connection failed', () async {
      when(client.get(
        any,
        headers: anyNamed('headers'),
      )).thenThrow(Exception());

       await expectLater(
          () async => await connectionServerDataSource.readConnection(
              username, password, appCId),
          throwsA(isInstanceOf<HttpException>()));
      verify(client.get(uri, headers: headers));
    });

    test('Should throw when read failed 401', () async {
      when(client.get(
        any,
        headers: anyNamed('headers'),
      )).thenAnswer((_) async => Response('', 401));

      await expectLater(
          () async => await connectionServerDataSource.readConnection(
              username, password, appCId),
          throwsA(isInstanceOf<ConnectionAuthenticationException>()));
      verify(client.get(uri, headers: headers));
    });

    test('Should throw when read failed 404', () async {
      when(client.get(
        any,
        headers: anyNamed('headers'),
      )).thenAnswer((_) async => Response('', 404));

      await expectLater(
          () async => await connectionServerDataSource.readConnection(
              username, password, appCId),
          throwsA(isInstanceOf<ConnectionApplicationNotFoundException>()));
      verify(client.get(uri, headers: headers));
    });
  });
  //
  group('Update Connection', () {
    test('Should call successful update connection', () async {
      when(client.put(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenAnswer((_) async => Response('', 200));

      await connectionServerDataSource.updateConnection(
          username, password, appCId, {'DeviceType': 'Android'});

      verify(client.put(uri, headers: headers, body: body));
      verify(applicationSettingsRepository.getApplicationSettings());
    });

    test('Should throw when http connection failed', () async {
      when(client.put(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenThrow(Exception());

      await expectLater(
          () async => await connectionServerDataSource.updateConnection(
              username, password, appCId, {'DeviceType': 'Android'}),
          throwsA(isInstanceOf<HttpException>()));
      verify(client.put(any,
          headers: anyNamed('headers'), body: anyNamed('body')));
    });

    test('Should throw when update failed 401', () async {
      when(client.put(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenAnswer((_) async => Response('', 401));

      await expectLater(
          () async => await connectionServerDataSource.updateConnection(
              username, password, appCId, {'DeviceType': 'Android'}),
          throwsA(isInstanceOf<ConnectionAuthenticationException>()));
      verify(client.put(any,
          headers: anyNamed('headers'), body: anyNamed('body')));
    });

    test('Should throw when update failed 404', () async {
      when(client.put(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenAnswer((_) async => Response('', 404));

      await expectLater(
          () async => await connectionServerDataSource.updateConnection(
              username, password, appCId, {'DeviceType': 'Android'}),
          throwsA(isInstanceOf<ConnectionApplicationNotFoundException>()));
      verify(client.put(any,
          headers: anyNamed('headers'), body: anyNamed('body')));
    });
  });
}
