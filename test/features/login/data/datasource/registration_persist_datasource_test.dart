import 'dart:convert';

import 'package:flutter_string_encryption/flutter_string_encryption.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_siloboard/features/logon/data/datasources/registration_persist_datasource.dart';
import 'package:mobile_siloboard/features/logon/data/model/registration_model.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

class MockPlatformStringCryptor extends Mock implements PlatformStringCryptor {}

class MackJsonCodec extends Mock implements JsonCodec {}

void main() {
  MockSharedPreferences sharedPreferences;
  MockPlatformStringCryptor platformStringCryptor;
  MackJsonCodec jsonCodec;
  RegistrationEncryptedPersistDataSource
      credentialEncryptedStoreSharedPreferencesDataSource;

  final RegistrationModel registrationModel = RegistrationModel(
    appCId: 'test',
    password: 'test',
    username: 'test',
  );
  final String passcode = '12345';
  final String salt = 'salt_123';
  final String encryptKey = '123123123';
  final String jsonRegistrationModel = 'json string';
  final String cryptedStr = '12345678';

  setUp(() {
    sharedPreferences = MockSharedPreferences();
    platformStringCryptor = MockPlatformStringCryptor();
    jsonCodec = MackJsonCodec();
    credentialEncryptedStoreSharedPreferencesDataSource =
        CredentialEncryptedStoreSharedPreferencesDataSource(
      json: jsonCodec,
      cryptor: platformStringCryptor,
      sharedPreferences: sharedPreferences,
    );
  });

  group('Store registration data', () {
    test('Should call encrypt and store mathods', () async {
      when(platformStringCryptor.generateSalt()).thenAnswer((_) async => salt);
      when(platformStringCryptor.generateKeyFromPassword(passcode, salt))
          .thenAnswer((_) async => encryptKey);
      when(platformStringCryptor.encrypt(jsonRegistrationModel, encryptKey))
          .thenAnswer((_) async => cryptedStr);
      when(jsonCodec.encode(registrationModel))
          .thenReturn(jsonRegistrationModel);
      when(sharedPreferences.setString(any, any)).thenAnswer((_) async => true);

      await credentialEncryptedStoreSharedPreferencesDataSource.store(
          registrationModel, passcode);

      verify(platformStringCryptor.generateSalt());
      verify(platformStringCryptor.generateKeyFromPassword(passcode, salt));
      verify(platformStringCryptor.encrypt(jsonRegistrationModel, encryptKey));
      verify(jsonCodec.encode(registrationModel));
      verify(sharedPreferences.setString(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH,
          cryptedStr));
      verify(sharedPreferences.setString(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_SALT, salt));
    });

    test('Should throw exeption when encrypt failed', () async {
      when(platformStringCryptor.encrypt(any, any))
          .thenThrow(MacMismatchException());

      expect(
          () async => credentialEncryptedStoreSharedPreferencesDataSource.store(
              registrationModel, passcode),
          throwsException);
    });

    test('Should throw exeption when store failed', () async {
      when(sharedPreferences.setString(any, any))
          .thenAnswer((_) async => false);

      expect(
          () async => credentialEncryptedStoreSharedPreferencesDataSource.store(
              registrationModel, passcode),
          throwsException);
    });

    test('Should throw exeption when json encode failed', () async {
      when(jsonCodec.encode(any)).thenThrow(Exception());

      expect(
          () async => credentialEncryptedStoreSharedPreferencesDataSource.store(
              registrationModel, passcode),
          throwsException);
    });
  });

  group('Restore registration data', () {
    test('Should call decript and restore mathods and get stored result',
        () async {
      when(sharedPreferences.getString(
              CredentialEncryptedStoreSharedPreferencesDataSource.TAG_SALT))
          .thenReturn(salt);
      when(sharedPreferences.getString(
              CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH))
          .thenReturn(cryptedStr);
      when(platformStringCryptor.generateKeyFromPassword(passcode, salt))
          .thenAnswer((_) async => encryptKey);
      when(platformStringCryptor.decrypt(cryptedStr, encryptKey))
          .thenAnswer((_) async => jsonRegistrationModel);
      when(jsonCodec.decode(jsonRegistrationModel))
          .thenReturn(registrationModel.toJson());

      final RegistrationModel restoredModel =
          await credentialEncryptedStoreSharedPreferencesDataSource
              .restore(passcode);
      expect(restoredModel, registrationModel);

      verify(sharedPreferences.getString(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_SALT));
      verify(sharedPreferences.getString(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH));
      verify(platformStringCryptor.generateKeyFromPassword(passcode, salt));
      verify(platformStringCryptor.decrypt(cryptedStr, encryptKey));
      verify(jsonCodec.decode(jsonRegistrationModel));
    });

    test('Should throw exeption when decrypt failed', () async {
      when(platformStringCryptor.decrypt(any, any))
          .thenThrow(MacMismatchException());

      expect(
          () async => credentialEncryptedStoreSharedPreferencesDataSource
              .restore(passcode),
          throwsException);
    });

    test('Should throw exeption when store failed', () async {
      when(sharedPreferences.getString(any)).thenThrow(Exception());

      expect(
          () async => credentialEncryptedStoreSharedPreferencesDataSource
              .restore(passcode),
          throwsException);
    });

    test('Should throw exeption when json encode failed', () async {
      when(jsonCodec.decode(any)).thenThrow(Exception());
      expect(
          () async => credentialEncryptedStoreSharedPreferencesDataSource
              .restore(passcode),
          throwsException);
    });
  });

  group('Check stored data', () {
    test('Should return true', () async {
      when(sharedPreferences.containsKey(
              CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH))
          .thenReturn(true);
      final bool result =
          await credentialEncryptedStoreSharedPreferencesDataSource.hasStored();
      expect(result, true);
      verify(sharedPreferences.containsKey(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH));
    });
    test('Should return false', () async {
      when(sharedPreferences.containsKey(
              CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH))
          .thenReturn(false);

      final bool result =
          await credentialEncryptedStoreSharedPreferencesDataSource.hasStored();
      expect(result, false);
      verify(sharedPreferences.containsKey(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH));
    });
  });

  group('Check remove data', () {
    test('Should be success', () async {
      when(sharedPreferences.remove(any)).thenAnswer((_) async => true);
      when(sharedPreferences.containsKey(any)).thenReturn(true);
      await credentialEncryptedStoreSharedPreferencesDataSource.remove();

      verify(sharedPreferences.remove(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH));
      verify(sharedPreferences.remove(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_SALT));
    });

    test('Should be failed id store is empty', () async {
      when(sharedPreferences.remove(any)).thenAnswer((_) async => false);
      when(sharedPreferences.containsKey(any)).thenReturn(false);
      await credentialEncryptedStoreSharedPreferencesDataSource.remove();
      verify(sharedPreferences.containsKey(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH));
      verifyNever(sharedPreferences.remove(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_SALT));
      verifyNever(sharedPreferences.remove(
          CredentialEncryptedStoreSharedPreferencesDataSource.TAG_AUTH));
    });

    test('Should be failed', () async {
      when(sharedPreferences.remove(any)).thenAnswer((_) async => false);
      when(sharedPreferences.containsKey(any)).thenReturn(true);
      expect(
          () async => await credentialEncryptedStoreSharedPreferencesDataSource
              .remove(),
          throwsException);
    });
  });
}
 