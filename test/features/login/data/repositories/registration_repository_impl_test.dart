import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_siloboard/features/logon/data/repositories/registration_repository_impl.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';

main(){

  LogonRepository logonRepository;
    final String username = 'username';
  final String password = 'password';
  final String applicationConnectionId = 'de4ece90-82a8-4ecf-b90c-10a9b18179da';

  final Registration registration = Registration(
      username: username, password: password, appCId: applicationConnectionId);

  setUp((){
      logonRepository = LogonRepositoryImpl();
  });

  test('Should be false after init', () {
    expect(logonRepository.hasRegistration(), false);
  });

  test('Should be true after set value', () {
    logonRepository.registration = registration;
    expect(logonRepository.hasRegistration(), true);
    expect(logonRepository.registration, registration);
  });

  test('Should be false after clear', () {
    logonRepository.registration = registration;
    logonRepository.clearRegistration();
     expect(logonRepository.hasRegistration(), false);
  });

}