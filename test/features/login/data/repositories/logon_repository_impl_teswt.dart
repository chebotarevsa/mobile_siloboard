import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_siloboard/features/logon/data/datasources/connection_server_datasource.dart';
import 'package:mobile_siloboard/features/logon/data/datasources/registration_persist_datasource.dart';
import 'package:mobile_siloboard/features/logon/data/model/connection_model.dart';
import 'package:mobile_siloboard/features/logon/data/model/registration_model.dart';
import 'package:mobile_siloboard/features/logon/data/repositories/logon_repository_impl.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/credential.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/passcode.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mockito/mockito.dart';

class MockConnectionServerDataSource extends Mock
    implements ConnectionServerDataSource {}

class MockRegistrationEncryptedPersistDataSource extends Mock
    implements RegistrationEncryptedPersistDataSource {}

main() {
  RegistrationRepository registrationRepository;
  ConnectionServerDataSource connectionServerDataSource;
  RegistrationEncryptedPersistDataSource registrationEncryptedPersistDataSource;

  final String username = 'username';
  final String password = 'password';
  final String applicationConnectionId = 'de4ece90-82a8-4ecf-b90c-10a9b18179da';
  final String passcodeStr = '1111';

  final Credential credential =
      Credential(username: username, password: password);
  final ConnectionModel connectionModel =
      ConnectionModel(applicationConnectionId: applicationConnectionId);
  final Registration registration = Registration(
      username: username, password: password, appCId: applicationConnectionId);

  final Passcode passcode = Passcode(passcodeStr);

  setUp(() {
    connectionServerDataSource = MockConnectionServerDataSource();
    registrationEncryptedPersistDataSource =
        MockRegistrationEncryptedPersistDataSource();
    registrationRepository = RegistrationRepositoryImpl(
        registrationEncryptedPersistDataSource:
            registrationEncryptedPersistDataSource,
        connectionServerDataSource: connectionServerDataSource);
  });

  group('Registration', () {
    test('Should be registration success', () async {
      when(connectionServerDataSource.createConnection(any, any))
          .thenAnswer((_) async => connectionModel);
      final Either<Failure, Registration> result =
          await registrationRepository.register(credential);
      final Either<Failure, Registration> success = Right(registration);
      expect(result, success);
      verify(connectionServerDataSource.createConnection(username, password));
    });

    test('Should be registration failed', () async {
      final Exception exeption =
          ConnectionException(message: 'Error', statusCode: 401);
      final Either<Failure, Success> failure =
          Left(Failure(exeption.toString(), reason: exeption));
      when(connectionServerDataSource.createConnection(any, any))
          .thenThrow(exeption);
      final Either<Failure, Registration> result =
          await registrationRepository.register(credential);
      expect(result, failure);
      verify(connectionServerDataSource.createConnection(username, password));
    });
  });

  group('Unregistration', () {
    test('Should be unregistred success', () async {
      when(connectionServerDataSource.deleteConnection(any, password, any))
          .thenAnswer((_) async => {});
      final Either<Failure, Success> result =
          await registrationRepository.unregister(registration);
      final Either<Failure, Success> success = Right(Success());
      expect(result, success);
      verify(connectionServerDataSource.deleteConnection(
          registration.username, registration.password, registration.appCId));
    });

    test('Should be unregistred failed', () async {
      final Exception exeption =
          ConnectionException(message: 'Error', statusCode: 401);
      final Either<Failure, Success> failure =
          Left(Failure(exeption.toString(), reason: exeption));
      when(connectionServerDataSource.deleteConnection(any, any, any))
          .thenThrow(exeption);
      final Either<Failure, Success> result =
          await registrationRepository.unregister(registration);
      expect(result, failure);
      verify(connectionServerDataSource.deleteConnection(
          registration.username, registration.password, registration.appCId));
    });
  });

  group('hasRegistration', () {
    test('Should be hasRegistration success and return true', () async {
      when(registrationEncryptedPersistDataSource.hasStored())
          .thenAnswer((_) async => true);
      final Either<Failure, bool> result =
          await registrationRepository.hasRegistration();
      final Either<Failure, bool> success = Right(true);
      expect(result, success);
      verify(registrationEncryptedPersistDataSource.hasStored());
    });

    test('Should be hasRegistration success and return false', () async {
      when(registrationEncryptedPersistDataSource.hasStored())
          .thenAnswer((_) async => false);
      final Either<Failure, bool> result =
          await registrationRepository.hasRegistration();
      final Either<Failure, bool> success = Right(false);
      expect(result, success);
      verify(registrationEncryptedPersistDataSource.hasStored());
    });

    test('Should be hasRegistration failed', () async {
      final Exception exeption = Exception('error');
      final Either<Failure, Success> failure =
          Left(Failure(exeption.toString(), reason: exeption));
      when(registrationEncryptedPersistDataSource.hasStored())
          .thenThrow(exeption);
      final Either<Failure, bool> result =
          await registrationRepository.hasRegistration();
      expect(result, failure);
      verify(registrationEncryptedPersistDataSource.hasStored());
    });
  });

  group('storeRegistration', () {
    RegistrationModel registrationModel = RegistrationModel(
      appCId: registration.appCId,
      password: registration.password,
      username: registration.username,
    );
    test('Should be storeRegistration success', () async {
      when(registrationEncryptedPersistDataSource.store(any, any))
          .thenAnswer((_) async => {});
      final Either<Failure, Success> result = await registrationRepository
          .storeRegistration(registration, passcode);
      final Either<Failure, Success> success = Right(Success());
      expect(result, success);
      verify(registrationEncryptedPersistDataSource.store(
          registrationModel, passcodeStr));
    });

    test('Should be storeRegistration failed', () async {
      final Exception exeption = Exception('error');
      final Either<Failure, Success> failure =
          Left(Failure(exeption.toString(), reason: exeption));
      when(registrationEncryptedPersistDataSource.store(any, any))
          .thenThrow(exeption);
      final Either<Failure, Success> result = await registrationRepository
          .storeRegistration(registration, passcode);
      expect(result, failure);
      verify(registrationEncryptedPersistDataSource.store(
          registrationModel, passcodeStr));
    });
  });

  group('restoreRegistration', () {
    RegistrationModel registrationModel = RegistrationModel(
      appCId: registration.appCId,
      password: registration.password,
      username: registration.username,
    );
    test('Should be restoreRegistration success', () async {
      when(registrationEncryptedPersistDataSource.restore(any))
          .thenAnswer((_) async => registrationModel);
      final Either<Failure, Registration> result =
          await registrationRepository.restoreRegistration(passcode);
      final Either<Failure, Registration> success = Right(registration);
      expect(result, success);
      verify(registrationEncryptedPersistDataSource.restore(passcodeStr));
    });

    test('Should be restoreRegistration failed', () async {
      final Exception exeption = Exception('error');
      final Either<Failure, Success> failure =
          Left(Failure(exeption.toString(), reason: exeption));
      when(registrationEncryptedPersistDataSource.restore(any))
          .thenThrow(exeption);
      final Either<Failure, Registration> result =
          await registrationRepository.restoreRegistration(passcode);
      expect(result, failure);
      verify(registrationEncryptedPersistDataSource.restore(passcodeStr));
    });
  });

  group('removeRegistration', () {
    test('Should be removeRegistration success', () async {
      when(registrationEncryptedPersistDataSource.remove())
          .thenAnswer((_) async => {});
      final Either<Failure, Success> result =
          await registrationRepository.removeRegistration();
      final Either<Failure, Success> success = Right(Success());
      expect(result, success);
      verify(registrationEncryptedPersistDataSource.remove());
    });

    test('Should be removeRegistration failed', () async {
      final Exception exeption = Exception('error');
      final Either<Failure, Success> failure =
          Left(Failure(exeption.toString(), reason: exeption));
      when(registrationEncryptedPersistDataSource.remove()).thenThrow(exeption);
      final Either<Failure, Success> result =
          await registrationRepository.removeRegistration();
      expect(result, failure);
      verify(registrationEncryptedPersistDataSource.remove());
    });
  });
}
