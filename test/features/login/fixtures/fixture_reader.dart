import 'dart:io';

String fixture(String name) => File('test/features/login/fixtures/$name').readAsStringSync();