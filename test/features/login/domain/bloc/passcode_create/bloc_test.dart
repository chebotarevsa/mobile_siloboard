import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc//bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/passcode_create/bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/passcode_create/event.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/passcode_create/state.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/passcode.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/l10n/localization.dart';
import 'package:mockito/mockito.dart';

class MockLogonRepository extends Mock implements LogonRepository {}

class MockRegistrationRepository extends Mock
    implements RegistrationRepository {}

class MockAppLocalization extends Mock implements AppLocalization {}

main() {
  LogonRepository logonRepository;
  RegistrationRepository registrationRepository;
  PasscodeCreateBloc passcodeCreateBloc;
  AppLocalization appLocalization;

  final String logonMessagePasscodeEmpty = 'logonMessagePasscodeEmpty';
  final String logonMessagePasscodeTooShorte = 'logonMessagePasscodeTooShorte';
  final String logonMessagePasscodeNotMatch = 'logonMessagePasscodeNotMatch';

  PasscodeCreateState initialState = PasscodeCreateState.init().copyWith(
      errorMassageValidatePasscode1: logonMessagePasscodeEmpty,
      errorMassageValidatePasscode2: logonMessagePasscodeEmpty);

  setUp(() {
    registrationRepository = MockRegistrationRepository();
    logonRepository = MockLogonRepository();
    appLocalization = MockAppLocalization();

    when(appLocalization.logonMessagePasscodeEmpty())
        .thenReturn(logonMessagePasscodeEmpty);
    when(appLocalization.logonMessagePasscodeTooShorte(any))
        .thenReturn(logonMessagePasscodeTooShorte);
    when(appLocalization.logonMessagePasscodeNotMatch())
        .thenReturn(logonMessagePasscodeNotMatch);

    passcodeCreateBloc = PasscodeCreateBloc(
        appLocalization: appLocalization,
        logonRepository: logonRepository,
        registrationRepository: registrationRepository);
  });
  tearDown(() {
    passcodeCreateBloc.close();
  });

  group('Initialization', () {
    test('Initialisation state', () {
      expect(passcodeCreateBloc.initialState, initialState);
    });
  });

  group('Validate passcode1', () {
    blocTest(
        'emits [initial, valid passcode1] when PasscodeCreateChanged1 is added',
        build: () => passcodeCreateBloc,
        act: (bloc) => bloc.add(PasscodeCreateChanged1(passcode1: '12345')),
        expect: [
          initialState,
          initialState.copyWith(
              passcode1: '12345',
              isValidPasscode1: true,
              isAutoValidatePasscode1: true,
              errorMassageValidatePasscode1: PasscodeCreateBloc.EMPTY)
        ]);
    blocTest(
        'emits [initial, invalid passcode1 ПИН-Код неньше 4 символов ] when PasscodeCreateChanged1 is added',
        build: () => passcodeCreateBloc,
        act: (bloc) => bloc.add(PasscodeCreateChanged1(passcode1: '123')),
        expect: [
          initialState,
          initialState.copyWith(
              passcode1: '123',
              isValidPasscode1: false,
              isAutoValidatePasscode1: true,
              errorMassageValidatePasscode1: logonMessagePasscodeTooShorte)
        ]);

    blocTest(
        'emits [initial, invalid passcode1 ПИН-Код не введен ] when PasscodeCreateChanged1 is added',
        build: () => passcodeCreateBloc,
        act: (bloc) => bloc.add(PasscodeCreateChanged1(passcode1: '')),
        expect: [
          initialState,
          initialState.copyWith(
              passcode1: '',
              isValidPasscode1: false,
              isAutoValidatePasscode1: true,
              errorMassageValidatePasscode1: logonMessagePasscodeEmpty)
        ]);
  });

  group('Validate passcode2', () {
    blocTest(
        'emits [initial, valid passcode1, valid passcode2] when PasscodeCreateChanged2 is added',
        build: () => passcodeCreateBloc,
        act: (bloc) async {
          bloc.add(PasscodeCreateChanged1(passcode1: '12345'));
          bloc.add(PasscodeCreateChanged2(passcode2: '12345'));
        },
        expect: [
          initialState,
          initialState.copyWith(
              passcode1: '12345',
              isValidPasscode1: true,
              isAutoValidatePasscode1: true,
              errorMassageValidatePasscode1: PasscodeCreateBloc.EMPTY),
          initialState.copyWith(
              passcode1: '12345',
              passcode2: '12345',
              isValidPasscode1: true,
              isValidPasscode2: true,
              isAutoValidatePasscode2: true,
              isAutoValidatePasscode1: true,
              errorMassageValidatePasscode1: PasscodeCreateBloc.EMPTY,
              errorMassageValidatePasscode2: PasscodeCreateBloc.EMPTY)
        ]);

    blocTest(
        'emits [initial,valid  invalid passcode2 ПИН-Код не введен ] when PasscodeCreateChanged2 is added',
        build: () => passcodeCreateBloc,
        act: (bloc) => bloc.add(PasscodeCreateChanged2(passcode2: '')),
        expect: [
          initialState,
          initialState.copyWith(
              passcode2: '',
              isValidPasscode2: false,
              isAutoValidatePasscode2: true,
              errorMassageValidatePasscode2: logonMessagePasscodeEmpty)
        ]);

    blocTest(
        'emits [initial,valid passcode1, invalid passcode2 ПИН-Коды не совпадают ] when PasscodeCreateChanged2 is added',
        build: () => passcodeCreateBloc,
        act: (bloc) async {
          bloc.add(PasscodeCreateChanged1(passcode1: '12345'));
          bloc.add(PasscodeCreateChanged2(passcode2: '12346'));
        },
        expect: [
          initialState,
          initialState.copyWith(
              passcode1: '12345',
              isValidPasscode1: true,
              isAutoValidatePasscode1: true,
              errorMassageValidatePasscode1: PasscodeCreateBloc.EMPTY),
          initialState.copyWith(
              passcode1: '12345',
              passcode2: '12346',
              isValidPasscode1: true,
              isValidPasscode2: false,
              isAutoValidatePasscode1: true,
              isAutoValidatePasscode2: true,
              errorMassageValidatePasscode1: PasscodeCreateBloc.EMPTY,
              errorMassageValidatePasscode2: logonMessagePasscodeNotMatch)
        ]);
  });

  group('Create passcode', () {
    final String username = 'username';
    final String password = 'password';
    final String applicationConnectionId =
        'de4ece90-82a8-4ecf-b90c-10a9b18179da';
    final String passcode = '1234';

    final Registration registration = Registration(
        username: username,
        password: password,
        appCId: applicationConnectionId);
    blocTest(
        'emits [initial, created Passcode] when PasscodeCreateSubmit is added',
        build: () {
      when(logonRepository.registration).thenReturn(registration);
      when(registrationRepository.storeRegistration(
        registration,
        Passcode(passcode),
      )).thenAnswer((_) async => Right(Success()));
      return passcodeCreateBloc;
    }, act: (bloc) async {
      bloc.add(PasscodeCreateSubmit(passcode1: passcode, passcode2: passcode));
    }, expect: [
      initialState,
      initialState.copyWith(
        createPasscodeStart: true,
        createPasscodeFailure: false,
        createPasscodeSuccess: false,
      ),
      initialState.copyWith(
        createPasscodeStart: false,
        createPasscodeFailure: false,
        createPasscodeSuccess: true,
      ),
      initialState.copyWith(
        createPasscodeStart: false,
        createPasscodeFailure: false,
        createPasscodeSuccess: false,
        errorMessageCreatePasscode: '',
      ),
    ]);

    blocTest(
        'emits [initial, failed created Passcode] when PasscodeCreateSubmit is added',
        build: () {
      when(logonRepository.registration).thenReturn(registration);
      when(registrationRepository.storeRegistration(
        registration,
        Passcode(passcode),
      )).thenAnswer((_) async => Left(Failure('Failure')));
      return passcodeCreateBloc;
    }, act: (bloc) async {
      bloc.add(PasscodeCreateSubmit(passcode1: passcode, passcode2: passcode));
    }, expect: [
      initialState,
      initialState.copyWith(
        createPasscodeStart: true,
        createPasscodeFailure: false,
        createPasscodeSuccess: false,
      ),
      initialState.copyWith(
        createPasscodeStart: false,
        createPasscodeFailure: true,
        createPasscodeSuccess: false,
        errorMessageCreatePasscode: 'Failure',
      ),
      initialState.copyWith(
        createPasscodeStart: false,
        createPasscodeFailure: false,
        createPasscodeSuccess: false,
        errorMessageCreatePasscode: '',
      ),
    ]);
  });

  group('Cancel create passcode', () {
    final String username = 'username';
    final String password = 'password';
    final String applicationConnectionId =
        'de4ece90-82a8-4ecf-b90c-10a9b18179da';

    final Registration registration = Registration(
        username: username,
        password: password,
        appCId: applicationConnectionId);

    blocTest(
        'emits [initial, created Passcode cancel success] when PasscodeCreateCancel is added',
        build: () {
          when(logonRepository.registration).thenReturn(registration);
          when(registrationRepository.unregister(registration))
              .thenAnswer((_) async => Right(Success()));
          return passcodeCreateBloc;
        },
        act: (bloc) => bloc.add(PasscodeCreateCancel()),
        expect: [
          initialState,
          initialState.copyWith(
            unregisterStart: true,
            unregisterSuccess: false,
            unregisterFailure: false,
          ),
          initialState.copyWith(
            unregisterStart: false,
            unregisterSuccess: true,
            unregisterFailure: false,
          ),
          initialState.copyWith(
            unregisterStart: false,
            unregisterFailure: false,
            unregisterSuccess: false,
            errorMessageUnregister: '',
          ),
        ]);

    blocTest(
        'emits [initial,  created Passcode cancel failed] when PasscodeCreateCancel is added',
        build: () {
          when(logonRepository.registration).thenReturn(registration);
          when(registrationRepository.unregister(registration))
              .thenAnswer((_) async => Left(Failure('Failure')));
          return passcodeCreateBloc;
        },
        act: (bloc) => bloc.add(PasscodeCreateCancel()),
        expect: [
          initialState,
          initialState.copyWith(
            unregisterStart: true,
            unregisterSuccess: false,
            unregisterFailure: false,
          ),
          initialState.copyWith(
              unregisterStart: false,
              unregisterSuccess: false,
              unregisterFailure: true,
              errorMessageUnregister: 'Failure'),
          initialState.copyWith(
            unregisterStart: false,
            unregisterFailure: false,
            unregisterSuccess: false,
            errorMessageUnregister: '',
          ),
        ]);
  });
}
