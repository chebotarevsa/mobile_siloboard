import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/passcode_validate/bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/passcode_validate/event.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/passcode_validate/state.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/passcode.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/l10n/localization.dart';
import 'package:mockito/mockito.dart';

class MockLogonRepository extends Mock implements LogonRepository {}

class MockRegistrationRepository extends Mock
    implements RegistrationRepository {}

class MockAppLocalization extends Mock implements AppLocalization {}

main() {
  PasscodeValidateBloc passcodeValidateBloc;
  LogonRepository logonRepository;
  RegistrationRepository registrationRepository;
  AppLocalization appLocalization;

  final String logonMessagePasscodeEmpty = 'logonMessagePasscodeEmpty';

  final String username = 'username';
  final String password = 'password';
  final String applicationConnectionId = 'de4ece90-82a8-4ecf-b90c-10a9b18179da';

  final Registration registration = Registration(
      username: username, password: password, appCId: applicationConnectionId);

  PasscodeValidateState initialState = PasscodeValidateState.init().copyWith(
    errorMessageValidateInputPasscode: logonMessagePasscodeEmpty,
  );

  setUp(() {
    logonRepository = MockLogonRepository();
    registrationRepository = MockRegistrationRepository();
    appLocalization = MockAppLocalization();

    when(appLocalization.logonMessagePasscodeEmpty())
        .thenReturn(logonMessagePasscodeEmpty);

    passcodeValidateBloc = PasscodeValidateBloc(
        appLocalization: appLocalization,
        logonRepository: logonRepository,
        registrationRepository: registrationRepository);
  });

  tearDown(() {
    passcodeValidateBloc.close();
  });

  group('Initialization', () {
    test('Should be initialized state', () {
      expect(passcodeValidateBloc.initialState, initialState);
    });
  });

  group('Submit passcode', () {
    test('Submit passcode failed', () async {
      when(registrationRepository.restoreRegistration(any))
          .thenAnswer((_) async => Left(Failure('Error')));

      passcodeValidateBloc.add(PasscodeValidateSubmit(passcode: '1111'));
      await emitsExactly(
        passcodeValidateBloc,
        [
          initialState,
          initialState.copyWith(
            validatePasscodeStart: true,
            validatePasscodeFailure: false,
            validatePasscodeSuccess: false,
          ),
          initialState.copyWith(
              validatePasscodeStart: false,
              validatePasscodeFailure: true,
              validatePasscodeSuccess: false,
              errorMessageValidatePasscode: 'Error'),
          initialState.copyWith(
            validatePasscodeStart: false,
            validatePasscodeFailure: false,
            validatePasscodeSuccess: false,
            errorMessageValidatePasscode: PasscodeValidateBloc.EMPTY,
          )
        ],
      );
      verify(registrationRepository.restoreRegistration(Passcode('1111')));
      verifyNever(logonRepository.registration = registration);
    });

    test('Submit passcode success', () async {
      when(registrationRepository.restoreRegistration(any))
          .thenAnswer((_) async => Right(registration));

      passcodeValidateBloc.add(PasscodeValidateSubmit(passcode: '1111'));
      await emitsExactly(
        passcodeValidateBloc,
        [
          initialState,
          initialState.copyWith(
            validatePasscodeStart: true,
            validatePasscodeFailure: false,
            validatePasscodeSuccess: false,
          ),
          initialState.copyWith(
            validatePasscodeStart: false,
            validatePasscodeFailure: false,
            validatePasscodeSuccess: true,
          ),
          initialState.copyWith(
            validatePasscodeStart: false,
            validatePasscodeFailure: false,
            validatePasscodeSuccess: false,
            errorMessageValidatePasscode: PasscodeValidateBloc.EMPTY,
          )
        ],
      );
      verify(logonRepository.registration = registration);
      verify(registrationRepository.restoreRegistration(Passcode('1111')));
    });
  });

  group('Forget passcode', () {
    test('Forget passcode success', () async {
      when(logonRepository.clearRegistration()).thenAnswer((_) {});
      when(registrationRepository.removeRegistration())
          .thenAnswer((_) async => Right(Success()));

      passcodeValidateBloc..add(PasscodeValidateForget());
      await emitsExactly(
        passcodeValidateBloc,
        [
          initialState,
          initialState.copyWith(
            removeRegistrationFailure: false,
            removeRegistrationStart: true,
            removeRegistrationSuccess: false,
            errorMessageRemoveRegistration: PasscodeValidateBloc.EMPTY,
          ),
          initialState.copyWith(
            removeRegistrationFailure: false,
            removeRegistrationStart: false,
            removeRegistrationSuccess: true,
            errorMessageRemoveRegistration: PasscodeValidateBloc.EMPTY,
          ),
          initialState.copyWith(
            removeRegistrationFailure: false,
            removeRegistrationStart: false,
            removeRegistrationSuccess: false,
            errorMessageRemoveRegistration: PasscodeValidateBloc.EMPTY,
          )
        ],
      );
      verify(logonRepository.clearRegistration());
      verify(registrationRepository.removeRegistration());
    });

    test('Forget passcode failed', () async {
      when(logonRepository.clearRegistration()).thenAnswer((_) {});
      when(registrationRepository.removeRegistration())
          .thenAnswer((_) async => Left(Failure('Error')));

      passcodeValidateBloc..add(PasscodeValidateForget());
      await emitsExactly(
        passcodeValidateBloc,
        [
          initialState,
          initialState.copyWith(
            removeRegistrationFailure: false,
            removeRegistrationStart: true,
            removeRegistrationSuccess: false,
            errorMessageRemoveRegistration: PasscodeValidateBloc.EMPTY,
          ),
          initialState.copyWith(
            removeRegistrationFailure: true,
            removeRegistrationStart: false,
            removeRegistrationSuccess: false,
            errorMessageRemoveRegistration: 'Error',
          ),
          initialState.copyWith(
            removeRegistrationFailure: false,
            removeRegistrationStart: false,
            removeRegistrationSuccess: false,
            errorMessageRemoveRegistration: PasscodeValidateBloc.EMPTY,
          )
        ],
      );
      verify(logonRepository.clearRegistration());
      verify(registrationRepository.removeRegistration());
    });
  });
}
