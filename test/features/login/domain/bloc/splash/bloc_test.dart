import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/splash/bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/splash/event.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc/splash/state.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mockito/mockito.dart';

class MockLogonRepository extends Mock implements LogonRepository {}

class MockRegistrationRepository extends Mock
    implements RegistrationRepository {}

main() {
  LogonRepository logonRepository;
  RegistrationRepository registrationRepository;
  SplashBloc splashBloc;

  group('Splash Bloc', () {
    setUp(() {
      logonRepository = MockLogonRepository();
      registrationRepository = MockRegistrationRepository();
      splashBloc = SplashBloc(
          logonRepository: logonRepository,
          registrationRepository: registrationRepository);
    });

    tearDown(() {
      splashBloc.close();
    });

    test('initial state', () {
      expect(splashBloc.initialState, SplashState.UNDEFINED);
    });

    blocTest(
      'emits [UNDEFINED, FAILED] when SplashLogonStarted is added and repository failure',
      build: () {
        when(logonRepository.hasRegistration()).thenReturn(false);
        when(registrationRepository.hasRegistration())
            .thenAnswer((_) async => Left(Failure('')));
        return splashBloc;
      },
      act: (bloc) => bloc.add(SplashLogonStarted()),
      expect: [SplashState.UNDEFINED, SplashState.FAILED],
    );
    blocTest(
      'emits [UNDEFINED, REGISTRED] when SplashLogonStarted is added and repository  has registration',
      build: () {
        when(logonRepository.hasRegistration()).thenReturn(false);
        when(registrationRepository.hasRegistration())
            .thenAnswer((_) async => Right(true));
        return splashBloc;
      },
      act: (bloc) => bloc.add(SplashLogonStarted()),
      expect: [SplashState.UNDEFINED, SplashState.REGISTRED],
    );
    blocTest(
      'emits [UNDEFINED, UNREGISTRED] when SplashLogonStarted is added and repository has not registartion',
      build: () {
        when(logonRepository.hasRegistration()).thenReturn(false);
        when(registrationRepository.hasRegistration())
            .thenAnswer((_) async => Right(false));
        return splashBloc;
      },
      act: (bloc) => bloc.add(SplashLogonStarted()),
      expect: [SplashState.UNDEFINED, SplashState.UNREGISTRED],
    );

    blocTest(
      'emits [UNDEFINED, LOGINED] when SplashLogonStarted is added and user logined already',
      build: () {
        when(logonRepository.hasRegistration()).thenReturn(true);
        return splashBloc;
      },
      act: (bloc) => bloc.add(SplashLogonStarted()),
      expect: [SplashState.UNDEFINED, SplashState.LOGINED],
    );
  });
}
