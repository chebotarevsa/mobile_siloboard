import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_siloboard/features/logon/domain/bloc//bloc.dart';
import 'package:mobile_siloboard/features/logon/domain/entities/registration.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/logon_repository.dart';
import 'package:mobile_siloboard/features/logon/domain/repositories/registration_repository.dart';
import 'package:mobile_siloboard/l10n/localization.dart';
import 'package:mockito/mockito.dart';

class MockLogonRepository extends Mock implements LogonRepository {}

class MockRegistrationRepository extends Mock
    implements RegistrationRepository {}

class MockAppLocalization extends Mock implements AppLocalization {}

main() {
  LogonRepository logonRepository;
  RegistrationRepository registrationRepository;
  RegistrationBloc registrationBloc;
  AppLocalization appLocalization;

  final String username = 'username';
  final String password = 'password';
  final String applicationConnectionId = 'de4ece90-82a8-4ecf-b90c-10a9b18179da';
  final String logonMessageUsernameEmpty = 'logonMessageUsernameEmpty';
  final String logonMessagePasswordEmpty = 'logonMessagePasswordEmpty';
  final String logonMessageUsernameUnexpected =
      'logonMessageUsernameUnexpected';
  final String logonMessageUsernameTooLong = 'logonMessageUsernameTooLong';

  final Registration registration = Registration(
      username: username, password: password, appCId: applicationConnectionId);
  RegistrationState initstate = RegistrationState.init().copyWith(
    errorMessageValidateUsername: logonMessageUsernameEmpty,
    errorMessageValidPassword: logonMessagePasswordEmpty,
  );

  setUp(() {
    logonRepository = MockLogonRepository();
    registrationRepository = MockRegistrationRepository();
    appLocalization = MockAppLocalization();
    when(appLocalization.logonMessageUsernameEmpty())
        .thenReturn(logonMessageUsernameEmpty);
    when(appLocalization.logonMessagePasswordEmpty())
        .thenReturn(logonMessagePasswordEmpty);
    when(appLocalization.logonMessageUsernameUnexpected())
        .thenReturn(logonMessageUsernameUnexpected);
    when(appLocalization.logonMessageUsernameTooLong(any))
        .thenReturn(logonMessageUsernameTooLong);

    registrationBloc = RegistrationBloc(
        appLocalization: appLocalization,
        logonRepository: logonRepository,
        registrationRepository: registrationRepository);
  });

  tearDown(() {
    registrationBloc.close();
  });

  group('Initialisation', () {
    test('Initialization bloc', () {
      expect(registrationBloc.initialState, initstate);
    });
  });
  group('Username changed', () {
    blocTest(
      'emits [initial, UserName is empty] when RegistrationUsernameChanged is added',
      build: () => registrationBloc,
      act: (bloc) => bloc.add(RegistrationUsernameChanged(username: "")),
      expect: [
        initstate,
        initstate.copyWith(
          username: '',
          isValidUsername: false,
          isAutoValidateUsername: true,
          errorMessageValidateUsername: logonMessageUsernameEmpty,
        )
      ],
    );

    blocTest(
      'emits [initial, UserName exist unsupport characters] when RegistrationUsernameChanged is added',
      build: () => registrationBloc,
      act: (bloc) => bloc.add(RegistrationUsernameChanged(username: '78%')),
      expect: [
        initstate,
        initstate.copyWith(
          username: '78%',
          isValidUsername: false,
          isAutoValidateUsername: true,
          errorMessageValidateUsername: logonMessageUsernameUnexpected,
        )
      ],
    );

    blocTest(
      'emits [initial, UserName length more then 12] when RegistrationUsernameChanged is added',
      build: () => registrationBloc,
      act: (bloc) =>
          bloc.add(RegistrationUsernameChanged(username: 'QQQQQQQQQQQQR')),
      expect: [
        initstate,
        initstate.copyWith(
          username: 'QQQQQQQQQQQQR',
          isValidUsername: false,
          isAutoValidateUsername: true,
          errorMessageValidateUsername: logonMessageUsernameTooLong,
        )
      ],
    );

    blocTest(
      'emits [initial, UserName is OK] when RegistrationUsernameChanged is added',
      build: () => registrationBloc,
      act: (bloc) =>
          bloc.add(RegistrationUsernameChanged(username: 'QQQQQQQQQQQQ')),
      expect: [
        initstate,
        initstate.copyWith(
            username: 'QQQQQQQQQQQQ',
            isAutoValidateUsername: true,
            isValidUsername: true,
            errorMessageValidateUsername: RegistrationBloc.MESSAGE_EMPTY)
      ],
    );
  });

  group('Password changed', () {
    blocTest(
      'emits [initial, Password is empty] when RegistrationPasswordChanged is added',
      build: () => registrationBloc,
      act: (bloc) => bloc.add(RegistrationPasswordChanged(password: "")),
      expect: [
        initstate,
        initstate.copyWith(
          password: '',
          isValidPassword: false,
          isAutoValidatePassword: true,
          errorMessageValidPassword: logonMessagePasswordEmpty,
        )
      ],
    );
    blocTest(
      'emits [initial, Password is OK] when RegistrationPasswordChanged is added',
      build: () => registrationBloc,
      act: (bloc) => bloc.add(RegistrationPasswordChanged(password: "RRR")),
      expect: [
        initstate,
        initstate.copyWith(
          password: 'RRR',
          isValidPassword: true,
          isAutoValidatePassword: true,
          errorMessageValidPassword: RegistrationBloc.MESSAGE_EMPTY,
        )
      ],
    );
  });

  group('Registration  submit', () {
    blocTest(
      'emits [initial, RegistartionError ] when RegistrationSubmit is added',
      build: () {
        when(registrationRepository.register(any))
            .thenAnswer((_) async => Left(Failure('Error')));
        return registrationBloc;
      },
      act: (bloc) =>
          bloc.add(RegistrationSubmit(password: password, username: username)),
      expect: [
        initstate,
        initstate.copyWith(registrationStart: true),
        initstate.copyWith(
          registrationStart: false,
          registrationFailure: true,
          registrationErrorMessage: 'Error',
        ),
        initstate.copyWith(
          registrationStart: false,
          registrationFailure: false,
          registrationSuccess: false,
          registrationErrorMessage: '',
        )
      ],
    );
    blocTest(
      'emits [initial,Registration is OK] when RegistrationSubmit is added',
      build: () {
        when(registrationRepository.register(any))
            .thenAnswer((_) async => Right(registration));
        return registrationBloc;
      },
      act: (bloc) =>
          bloc.add(RegistrationSubmit(password: password, username: username)),
      expect: [
        initstate,
        initstate.copyWith(registrationStart: true),
        initstate.copyWith(
            registrationStart: false,
            registrationFailure: false,
            registrationSuccess: true),
        initstate.copyWith(
          registrationStart: false,
          registrationFailure: false,
          registrationSuccess: false,
          registrationErrorMessage: '',
        )
      ],
    );
  });
}
